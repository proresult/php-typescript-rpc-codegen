<?php
    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen\Tests;


    use PHPUnit\Framework\TestCase;
    use Proresult\PhpTypescriptRpc\Codegen\Php\NamespaceAdjuster;

    class NamespaceAdjusterTest extends TestCase {

        function testAdjust() {
            $base = '\\Proresult\\Namespace\\Adjust';
            $remove = '\\This\\Will\\Be\\Removed';
            $add = '\\This\\Was\\Added';
            $input = $base.$remove;
            $expect = $base.$add;
            $adjuster = new NamespaceAdjuster($remove, $add);
            $this->assertEquals($expect, $adjuster->adjust($input));

            $input = $input.'\\MoreOhMore';
            $expect = $expect.'\\MoreOhMore';
            $this->assertEquals($expect, $adjuster->adjust($input));

            $this->expectExceptionMessage("namespace \"\Proresult\Namespace\Adjust\Something\Else\" does not contain namespace part to remove (\This\Will\Be\Removed)");
            $adjuster->adjust($base."\\Something\\Else");
        }

    }