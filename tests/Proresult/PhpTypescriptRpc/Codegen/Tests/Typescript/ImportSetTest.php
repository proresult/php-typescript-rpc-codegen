<?php
    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen\Tests\Typescript;


    use PHPUnit\Framework\TestCase;
    use Proresult\PhpTypescriptRpc\Codegen\Typescript\Import;
    use Proresult\PhpTypescriptRpc\Codegen\Typescript\ImportSet;
    use Proresult\PhpTypescriptRpc\Codegen\Typescript\ModulePath;
    use Proresult\PhpTypescriptRpc\Codegen\Typescript\ModulePathPrefixAdjustment;
    use Proresult\PhpTypescriptRpc\Codegen\Typescript\ModulePathPrefixAdjustments;
    use Proresult\PhpTypescriptRpc\Codegen\Typescript\Name;

    class ImportSetTest extends TestCase {

        private function getBaseModulePath1(): ModulePath {
            return new ModulePath(Name::arrayFromStrings("Test", "Import", "Set"));
        }
        private function getBaseModulePath1Adjusted(): ModulePath {
            return new ModulePath(Name::arrayFromStrings("Result", "Adjusted", "Set"));
        }

        private function getInput1(): array {
            $baseModulePath = $this->getBaseModulePath1();
            return [
                new Import($baseModulePath->appendName(new Name("A")), new Name("A")),
                new Import($baseModulePath->appendName(new Name("B")), new Name("B")),
                new Import($baseModulePath->appendName(new Name("A")), new Name("A")),
                new Import($baseModulePath->appendName(new Name("A")), new Name("Aliased")),
            ];
        }

        private function getInput1BaseAdjusted(): array {
            $baseModulePath = $this->getBaseModulePath1Adjusted();
            return [
                new Import($baseModulePath->appendName(new Name("A")), new Name("A")),
                new Import($baseModulePath->appendName(new Name("B")), new Name("B")),
                new Import($baseModulePath->appendName(new Name("A")), new Name("A")),
                new Import($baseModulePath->appendName(new Name("A")), new Name("Aliased")),
            ];
        }

        private function getExpect1(): array {
            $baseModulePath = $this->getBaseModulePath1();
            return [
                new Import($baseModulePath->appendName(new Name("A")), new Name("A")),
                new Import($baseModulePath->appendName(new Name("B")), new Name("B")),
                new Import($baseModulePath->appendName(new Name("A")), new Name("Aliased")),
            ];
        }

        private function getInput2(): array {
            $baseModulePath1 = new ModulePath(Name::arrayFromStrings("Testing", "Is", "Good"));
            $baseModulePath2 = new ModulePath(Name::arrayFromStrings("This", "Works", "Well"));
            return [
                new Import($baseModulePath1, new Name("Yes")),
                new Import($baseModulePath2, new Name("Usually")),
            ];
        }

        public function testDuplicateAdd() {
            $set = new ImportSet();
            $imports = $this->getInput1();
            $expect = $this->getExpect1();
            $set->addImports($imports);
            $this->assertCount(3, $set->getImports());
            $this->assertEquals($expect, $set->getImports());

        }

        public function testClone() {
            $base = ImportSet::empty();
            $clone = $base->clone();
            $this->assertNotSame($clone, $base);
        }

        public function testConcat() {
            $base = ImportSet::empty();
            $base->addImports($this->getInput1());
            $other = ImportSet::empty();
            $result = $base->concat($other);
            $this->assertNotSame($base, $result);
            $this->assertEquals($base, $result);

            $other->addImports($this->getInput2());
            $result = $base->concat($other);
            $expect = ImportSet::empty();
            $expect->addImports($this->getInput1());
            $expect->addImports($this->getInput2());
            $this->assertNotSame($result, $expect);
            $this->assertEquals($result, $expect);
        }

        public function testInit() {
            $expect = new ImportSet();
            $expect->addImports($this->getInput1());
            $result = ImportSet::init(...$this->getInput1());
            $this->assertEquals($expect, $result);
        }

        public function testModulePathPrefixAdjust() {
            $input = ImportSet::init(...$this->getInput1())->concat(ImportSet::init(...$this->getInput2()));
            $adjust = new ModulePathPrefixAdjustments(new ModulePathPrefixAdjustment(
                remove: new ModulePath(Name::arrayFromStrings("Test", "Import")),
                add: new ModulePath(Name::arrayFromStrings("Result", "Adjusted"))
            ));
            $result = $input->withModulePathPrefixAdjusted($adjust);
            $expected = ImportSet::init(...$this->getInput1BaseAdjusted())->concat(ImportSet::init(...$this->getInput2()));
            $this->assertEquals($expected, $result);
        }
    }