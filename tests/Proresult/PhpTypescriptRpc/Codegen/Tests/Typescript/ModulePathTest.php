<?php
    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen\Tests\Typescript;


    use PHPUnit\Framework\TestCase;
    use Proresult\PhpTypescriptRpc\Codegen\Typescript\ModulePath;
    use Proresult\PhpTypescriptRpc\Codegen\Typescript\ModulePathPrefixAdjustment;
    use Proresult\PhpTypescriptRpc\Codegen\Typescript\Name;

    class ModulePathTest extends TestCase {

        public function testModulePathPrefixAdjustment() {
            $testCases = [
                [
                    "input" => ["A", "B", "C", "D"],
                    "remove" => ["A", "B"],
                    "add" => ["AX", "BX"],
                    "expect" => ["AX", "BX", "C", "D"],
                ],
                [
                    "input" => ["A", "B", "C", "D"],
                    "remove" => ["A"],
                    "add" => ["AX", "BX"],
                    "expect" => ["AX", "BX", "B", "C", "D"],
                ],
                // If nothing is to be removed, always add
                [
                    "input" => ["A", "B", "C", "D"],
                    "remove" => [],
                    "add" => ["AX", "BX"],
                    "expect" => ["AX", "BX", "A", "B", "C", "D"],
                ],
                // Don't add when nothing was removed:
                [
                    "input" => ["A", "B", "C", "D"],
                    "remove" => ["AX"],
                    "add" => ["BX"],
                    "expect" => ["A", "B", "C", "D"],
                ],
                // Remove even when nothing is to be added:
                [
                    "input" => ["A", "B", "C", "D"],
                    "remove" => ["A", "B"],
                    "add" => [],
                    "expect" => ["C", "D"],
                ],
                // Don't remove (or add) when it's not the prefix
                [
                    "input" => ["A", "B", "C", "D"],
                    "remove" => ["B", "C"],
                    "add" => ["AX", "BX"],
                    "expect" => ["A", "B", "C", "D"],
                ],
            ];

            for($i = 0; $i < count($testCases); $i++) {
                $testCase = $testCases[$i];

                $input = new ModulePath(Name::arrayFromStrings(...$testCase["input"]));
                $remove = new ModulePath(Name::arrayFromStrings(...$testCase["remove"]));
                $add = new ModulePath(Name::arrayFromStrings(...$testCase["add"]));
                $expect = new ModulePath(Name::arrayFromStrings(...$testCase["expect"]));

                $adjustment = new ModulePathPrefixAdjustment($remove, $add);
                $result = $adjustment->adjust($input);
                $this->assertEquals($expect, $result, "test case $i failed");

            }

        }
    }