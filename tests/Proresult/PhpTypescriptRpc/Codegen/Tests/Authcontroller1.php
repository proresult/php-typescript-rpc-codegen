<?php
    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen\Tests;

    use Proresult\PhpTypescriptRpc\Server\Exceptions\UnauthorizedException;

    class Authcontroller1 {
        public const DEFAULT_ALLOW_METHOD = "allow";
        public const REQUIRE_ADMIN = "admin";

        public function __construct(EnrichedRequest $request) {
            if (!$request->isAuthenticated()) {
                throw new UnauthorizedException();
            }
        }

        public function allow(): bool {
            return true;
        }

        public function deny(): bool {
            return false;
        }

        public function specialAuthorizedSome(int $what): bool {
            return $what > 10;
        }

        public function admin(): bool {
            return false;
        }

        public function projectLead(int $projectNum): bool {
            return $projectNum > 100;
        }

        public function classified(int $birthYearFrom, int $birthYearTo): bool {
            return $birthYearFrom < 2000 || $birthYearTo < 2000;
        }
    }