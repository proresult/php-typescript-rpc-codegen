<?php
    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen\Tests;


    use PHPUnit\Framework\TestCase;
    use Proresult\PhpTypescriptRpc\Codegen\Files;
    use Proresult\PhpTypescriptRpc\Codegen\Php\ClassReference;
    use Proresult\PhpTypescriptRpc\Codegen\Php\NamespaceAdjuster;
    use Proresult\PhpTypescriptRpc\Codegen\Php\NamespaceRep;
    use Proresult\PhpTypescriptRpc\Codegen\PhpRpcAdapterCoder;
    use Proresult\PhpTypescriptRpc\Codegen\PhpRpcAdapterGenerator;
    use phpDocumentor\Reflection\Php\File as PhpFile;

    class RpcAdapterGeneratorTest extends TestCase {

        private $inputBaseDir = __DIR__.'/Inputcode/Rpc';
        private $outputBaseDir = __DIR__.'/Generated/RpcAdapters';
        private NamespaceAdjuster $nsAdjuster;
        private NamespaceRep $baseNamespace;
        private PhpRpcAdapterGenerator $generator;

        /**
         * RpcAdapterGeneratorTest constructor.
         */
        public function __construct() {
            parent::__construct();
            $this->nsAdjuster = new NamespaceAdjuster("\\Inputcode\\Rpc", "\\Generated\\RpcAdapters");
            $this->baseNamespace = new NamespaceRep(["Proresult", "PhpTypescriptRpc", "Codegen", "Tests", "Inputcode", "Rpc"]);
            $this->generator = new PhpRpcAdapterGenerator($this->inputBaseDir, $this->outputBaseDir, $this->baseNamespace, $this->nsAdjuster, "");
        }


        function testHelloFile() {
            $rpcAdapterGenerator = $this->generator;
            $helloRpcFile = array_values(array_filter($rpcAdapterGenerator->getInputFiles(), fn(PhpFile $file) => $file->getName() == "HelloEndpoint.php"));
            $this->assertCount(1, $helloRpcFile);
            $helloRpcFile = $helloRpcFile[0];
            $rpcClass = $rpcAdapterGenerator->rpcClassForFile($helloRpcFile);
            $this->assertEquals("HelloEndpoint", $rpcClass->rpcClassRef->name);
            $this->assertEquals('Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Rpc', $rpcClass->rpcClassRef->namespace);
            $this->assertNull($rpcClass->requestClassRef);
            // $this->assertEquals('Proresult\PhpTypescriptRpc\Codegen\Tests', $rpcClass->requestEnricher->namespace);
            // $this->assertEquals('RequestEnricher', $rpcClass->requestEnricher->name);
            $this->assertCount(3, $rpcClass->rpcMethods->methods);
            $method = $rpcClass->rpcMethods->methods[0];
            $this->assertEquals("hello", $method->methodName);
            $this->assertEquals(new ClassReference('Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Models',"HelloRequest"), $method->arguments->first()->getClassReference());
            $this->assertEquals(new ClassReference('Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Models',"HelloResponse"), $method->responseType);
            $this->assertEquals(new ClassReference('Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Rpc',"HelloEndpoint"), $method->rpcClassRef);
            $this->assertEquals("/HelloEndpoint/hello", $method->rpcPath($this->baseNamespace));
        }

        function testGetFile() {
            $rpcAdapterGenerator = $this->generator;
            $getRpcFile = array_values(array_filter($rpcAdapterGenerator->getInputFiles(), fn(PhpFile $file) => $file->getName() == "GetEndpoint.php"));
            $this->assertCount(1, $getRpcFile);
            $getRpcFile = $getRpcFile[0];
            $rpcClass = $rpcAdapterGenerator->rpcClassForFile($getRpcFile);
            $this->assertEquals("GetEndpoint", $rpcClass->rpcClassRef->name);
            $this->assertEquals('Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Rpc', $rpcClass->rpcClassRef->namespace);
            $this->assertEquals('Proresult\PhpTypescriptRpc\Codegen\Tests', $rpcClass->requestClassRef->namespace);
            $this->assertEquals('EnrichedRequest', $rpcClass->requestClassRef->name);
            $this->assertCount(4, $rpcClass->rpcMethods->methods);
            $method = $rpcClass->rpcMethods->methods[0];
            $this->assertEquals("getSome", $method->methodName);
            $getSomeArgs = $method->arguments;
            $this->assertEquals($getSomeArgs->count(), 3);
            $txt1Arg = $getSomeArgs->arguments[0];
            $this->assertEquals("string", $txt1Arg->paramType);
            $this->assertEquals("txt1", $txt1Arg->paramName);
            $this->assertFalse($txt1Arg->isNullable());
            $txt2Arg = $getSomeArgs->arguments[1];
            $this->assertEquals("string", $txt2Arg->paramType);
            $this->assertEquals("txt2", $txt2Arg->paramName);
            $this->assertFalse($txt2Arg->isNullable());
            $num1Arg = $getSomeArgs->arguments[2];
            $this->assertEquals("int", $num1Arg->paramType);
            $this->assertEquals("num1", $num1Arg->paramName);
            $this->assertEquals(new ClassReference('Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Models',"GetSomeResponse"), $method->responseType);
            $this->assertEquals(new ClassReference('Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Rpc',"GetEndpoint"), $method->rpcClassRef);
            $this->assertEquals("/GetEndpoint/getSome", $method->rpcPath($this->baseNamespace));
            $this->assertNull($rpcClass->authTypeRef);
            // Inspect the getWithOptionalParam method
            $getWithOptionalParamMethod = $rpcClass->rpcMethods->methods[2];
            $this->assertEquals("getWithOptionalParam", $getWithOptionalParamMethod->methodName);
            $numOrNullArg = $getWithOptionalParamMethod->arguments->first();
            $this->assertEquals("numOrNull", $numOrNullArg->paramName);
            $this->assertEquals("int", $numOrNullArg->paramType);
            $this->assertTrue($numOrNullArg->isNullable());
        }

        function testAuthenticatedFile() {
            $rpcAdapterGenerator = $this->generator;
            $getRpcFile = array_values(array_filter($rpcAdapterGenerator->getInputFiles(), fn(PhpFile $file) => $file->getName() == "AuthenticatedRpc.php"));
            $this->assertCount(1, $getRpcFile);
            $getRpcFile = $getRpcFile[0];
            $rpcClass = $rpcAdapterGenerator->rpcClassForFile($getRpcFile);
            $this->assertEquals("AuthenticatedRpc", $rpcClass->rpcClassRef->name);
            $this->assertEquals('Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Rpc', $rpcClass->rpcClassRef->namespace);
            $this->assertEquals('Proresult\PhpTypescriptRpc\Codegen\Tests', $rpcClass->requestClassRef->namespace);
            $this->assertEquals('EnrichedRequest', $rpcClass->requestClassRef->name);
            $this->assertCount(2, $rpcClass->rpcMethods->methods);
            $method = $rpcClass->rpcMethods->methods[0];
            $this->assertEquals("getWithoutParams", $method->methodName);
            $getSomeArgs = $method->arguments;
            $this->assertEquals(0, $getSomeArgs->count());
            $this->assertEquals(new ClassReference('Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Models',"GetSomeResponse"), $method->responseType);
            $this->assertEquals(new ClassReference('Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Rpc',"AuthenticatedRpc"), $method->rpcClassRef);
            $this->assertEquals("/AuthenticatedRpc/getWithoutParams", $method->rpcPath($this->baseNamespace));
            $this->assertEquals(Authcontroller1::DEFAULT_ALLOW_METHOD, $rpcClass->authTypeRef->defaultMethodName);
            $this->assertEquals(Authcontroller1::class, $rpcClass->authTypeRef->controllerClass->__toString());
        }

        function testOutputFilepath() {
            $inputBase = "/some/root/input";
            $outputBase = "/some/root/output";
            $adjuster = new NamespaceAdjuster("", "");
            $generator = new PhpRpcAdapterGenerator($inputBase, $outputBase, $this->baseNamespace, $adjuster, "");

            $classSubdir = '/namespaceA/namespaceB/';
            $inputClassname = 'RpcClass';
            $expectClassname = 'RpcClassAdapter';
            $php = ".php";
            $input = $inputBase.$classSubdir.$inputClassname.$php;
            $expect = $outputBase.$classSubdir.$expectClassname.$php;
            $this->assertEquals($expect, $generator->outputFilepath($input, $expectClassname));
        }

        function testProcessAll() {
            $rpcAdapterGenerator = new PhpRpcAdapterGenerator($this->inputBaseDir, $this->outputBaseDir, $this->baseNamespace, $this->nsAdjuster, "");
            // Remove existing files in test output dir
            Files::rmdirRecursive($this->outputBaseDir, "php");
            $rpcAdapterGenerator->processAllFiles();
            $expectedCount = count($rpcAdapterGenerator->getInputFiles());
            $generatedFiles = Files::phpFileArray($this->outputBaseDir);
            $this->assertCount($expectedCount, $generatedFiles);
            $generatedFilePaths = array_map(fn(\SplFileInfo $file) => realpath($file->getPathname()), $generatedFiles);
            $expectedFile = realpath($this->outputBaseDir.'/HelloEndpointAdapter.php');
            $this->assertContains($expectedFile, $generatedFilePaths);
        }
    }