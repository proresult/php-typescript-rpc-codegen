<?php
    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen\Tests;

    use Exception;
    use Proresult\PhpTypescriptRpc\Server\ResponseFactoryInterface;
    use Psr\Http\Message\ResponseInterface;

    class TestResponseFactory implements ResponseFactoryInterface {

        public function newResponse(int $statusCode, ?string $body, ?string $contentType) : ResponseInterface {
            throw new Exception("Test triggered error response factory");
        }
    }