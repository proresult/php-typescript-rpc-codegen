<?php
    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen\Tests;


    use PHPUnit\Framework\TestCase;
    use Proresult\PhpTypescriptRpc\Codegen\Typescript\Name;

    class NameTest extends TestCase {

        public function testLegalCheck() {
            $legal = ["abba", "jostein", "Julius", "124", "000gugg", "@test", "aaa-bbb-ccc"];
            $illegal = ["abba babba", "jussu/knitt", "ji / aa", "a \ b", "/bb", "bb/", ".", "as.bb", "sd\nuu", "øæ"];

            foreach($legal as $input) {
                $this->assertTrue(Name::legalCheck($input), "$input should be legal, but Name::legalCheck returned false");
            }
            foreach($illegal as $input) {
                $this->assertFalse(Name::legalCheck($input), "$input should be illegal, but Name::legalCheck returned true");
            }
        }
    }