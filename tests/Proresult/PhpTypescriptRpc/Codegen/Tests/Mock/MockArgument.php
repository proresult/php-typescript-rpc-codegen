<?php
    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen\Tests\Mock;


    use phpDocumentor\Reflection\Php\Argument as PhpArgument;
    use phpDocumentor\Reflection\Php\Method as PhpMethod;
    use Proresult\PhpTypescriptRpc\Codegen\Typescript\Argument;
    use Proresult\PhpTypescriptRpc\Codegen\Typescript\TypescriptTypes;

    class MockArgument extends Argument {
        public string $_name;

        public function __construct(string $name, TypescriptTypes $types) {
            $this->_name = $name;
            $this->types = $types;
        }


        public function name(): string {
            return $this->_name;
        }

    }