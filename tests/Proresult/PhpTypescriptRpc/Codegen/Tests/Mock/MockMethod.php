<?php
    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen\Tests\Mock;

    use Proresult\PhpTypescriptRpc\Codegen\Typescript\Method;

    class MockMethod extends Method {
        public string $_name;

        /**
         * MockMethod constructor.
         *
         * @param string $name
         * @param MockArgument[]  $arguments
         */
        public function __construct(string $name, array $arguments) {
            $this->_name = $name;
            $this->args = $arguments;
        }

        public function name(): string {
            return $this->_name;
        }

    }