<?php
    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen\Tests\Mock;


    use Proresult\PhpTypescriptRpc\Codegen\Typescript\Property;
    use Proresult\PhpTypescriptRpc\Codegen\Typescript\TypescriptTypes;

    class MockProperty extends Property {
        public string $_name;

        /**
         * MockProperty constructor.
         *
         * @param string $name
         */
        public function __construct(string $name, ?TypescriptTypes $types) {
            $this->_name = $name;
            $this->types = $types;
        }

        public function name(): string {
            return $this->_name;
        }



    }