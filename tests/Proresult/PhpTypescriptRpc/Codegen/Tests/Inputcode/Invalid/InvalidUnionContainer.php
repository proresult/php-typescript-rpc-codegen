<?php
    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Invalid;


    use Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Models\Car;
    use Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Models\Person;

    class InvalidUnionContainer {
        public Car | Person $carOrPerson;
    }