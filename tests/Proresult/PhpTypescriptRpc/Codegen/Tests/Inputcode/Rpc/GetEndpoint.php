<?php
    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Rpc;

    use Proresult\PhpTypescriptRpc\Codegen\Tests\EnrichedRequest;
    use Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Models\GetSomeResponse;
    use Proresult\PhpTypescriptRpc\Server\Attributes\Get;
    use Proresult\PhpTypescriptRpc\Server\Attributes\RequestType;
    use Proresult\PhpTypescriptRpc\Server\Attributes\Retryable;

    #[RequestType(EnrichedRequest::class)]
    class GetEndpoint {

        static function initFromRequest(EnrichedRequest $request): GetEndpoint {
            return new GetEndpoint();
        }

        #[Get]
        public function getSome(string $txt1, string $txt2, int $num1 = 44): GetSomeResponse {
            return new GetSomeResponse($txt1, $txt2, $num1);
        }

        #[Get]
        public function getWithoutParams(): GetSomeResponse {
            return new GetSomeResponse("a", "b", 3);
        }

        #[Get]
        #[Retryable(0)]
        public function getWithOptionalParam(?int $numOrNull = 666): GetSomeResponse {
            return new GetSomeResponse("a", "b", $numOrNull ?? 444);
        }

        #[Get]
        public function getWithNullableParam(string | null $strOrNull = null): GetSomeResponse {
            return new GetSomeResponse($strOrNull ?? "noStr", "b", 43);
        }
    }