<?php
/*
Copyright 2021, Proresult AS.
License: MIT
*/
declare(strict_types = 1);

namespace Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Rpc;

use Proresult\PhpTypescriptRpc\Codegen\Tests\Authcontroller1;
use Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Models\SecondHelloRequest;
use Proresult\PhpTypescriptRpc\Server\Attributes\AuthCheck;
use Proresult\PhpTypescriptRpc\Server\Attributes\AuthType;
use Proresult\PhpTypescriptRpc\Server\Attributes\Get;
use Proresult\PhpTypescriptRpc\Server\Attributes\RequestType;
use Proresult\PhpTypescriptRpc\Codegen\Tests\EnrichedRequest;
use Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Models\GetSomeResponse;

#[RequestType(EnrichedRequest::class)]
#[AuthType(Authcontroller1::class, "deny")]
class SecondAuthenticatedRpc {

    public function __construct(private int $requestId) {}

    static function initFromRequest(EnrichedRequest $request): SecondAuthenticatedRpc {
        return new SecondAuthenticatedRpc($request->getRequestId());
    }

    // Authcheck will be a call to Authcontroller1->allow(). (Set as default method in Authorization attribute)
    #[Get]
    public function getSome(string $a): GetSomeResponse {
        return new GetSomeResponse($a, "haba baba", $this->requestId);
    }

    // Authcheck will be a call to Authcontroller1->specialAuthenticatedSome($what). (Because no method name is given in AuthCheck)
    #[Get]
    #[AuthCheck]
    public function specialAuthorizedSome(int $what): GetSomeResponse {
        return new GetSomeResponse("aaa", "bbb", $what);
    }

    // Authcheck will be a call to Authcontroller1->admin()
    #[Get]
    #[AuthCheck(AuthController1::REQUIRE_ADMIN)]
    public function adminRoleRequiredSome(): GetSomeResponse {
        return new GetSomeResponse("admin", "only", 333);
    }

    // Authcheck will be a call to Authcontroller1->projectLead($projectNum)
    #[Get]
    #[AuthCheck("projectLead", true)]
    public function projectLeadRequired(int $projectNum): GetSomeResponse {
        return new GetSomeResponse("project lead", "only", $projectNum);
    }

    // Authcheck will be a call to Authcontroller1->classified($hello->to->firstName, $hello->to->drives->year)
    #[AuthCheck("classified", true, 'hello->to->firstName', 'hello->to->drives->year')]
    public function classifiedHello(SecondHelloRequest $hello): GetSomeResponse {
        return new GetSomeResponse("hello", $hello->to->firstName, $hello->to->drives->year);
    }
}