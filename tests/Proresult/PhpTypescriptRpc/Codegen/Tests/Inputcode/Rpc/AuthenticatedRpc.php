<?php
/*
Copyright 2021, Proresult AS.
License: MIT
*/
declare(strict_types = 1);

namespace Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Rpc;

use Proresult\PhpTypescriptRpc\Codegen\Tests\Authcontroller1;
use Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Models\HelloRequest;
use Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Models\HelloResponse;
use Proresult\PhpTypescriptRpc\Server\Attributes\AuthType;
use Proresult\PhpTypescriptRpc\Server\Attributes\Get;
use Proresult\PhpTypescriptRpc\Server\Attributes\RequestType;
use Proresult\PhpTypescriptRpc\Codegen\Tests\EnrichedRequest;
use Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Models\GetSomeResponse;
use Proresult\PhpTypescriptRpc\Server\Models\RpcDateTime;

#[RequestType(EnrichedRequest::class)]
#[AuthType(Authcontroller1::class, Authcontroller1::DEFAULT_ALLOW_METHOD)]
class AuthenticatedRpc {

    public function __construct(private int $requestId) {}

    static function initFromRequest(EnrichedRequest $request): AuthenticatedRpc {
        return new AuthenticatedRpc($request->getRequestId());
    }

    #[Get]
    public function getWithoutParams(): GetSomeResponse {
        return new GetSomeResponse("a", "haba baba", $this->requestId);
    }

    public function hello(HelloRequest $request): HelloResponse {
        $response = new HelloResponse();
        $response->message = "Hello, {$request->name}";
        $response->helloTime = new RpcDateTime(new \DateTimeImmutable());
        return $response;
    }

}