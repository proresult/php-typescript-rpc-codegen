<?php
    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Rpc;


    use Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Models\CircularRef;
    use Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Models\GoodbyeRequest;
    use Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Models\GoodbyeResponse;
    use Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Models\HelloRequest;
    use Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Models\HelloResponse;
    use Proresult\PhpTypescriptRpc\Server\Attributes\Retryable;
    use Proresult\PhpTypescriptRpc\Server\Models\RpcDateTime;
    use Psr\Http\Message\ServerRequestInterface;

    class HelloEndpoint {

        /**
         * HelloEndpoint constructor. Dependencies the rpc methods need should be injected here.
         *
         */
        public function __construct() {
        }

        /**
         * All Rpc classes must have a initFromRequest function like this. So that the RpcAdapter that is generated can initialize new Rpc instances on incoming requests.
         * This should do all initialization the rpc needs, and pass needed resources to the class constructor as dependency injection.
         * This is then effectively the prod init of the rpc, while the constructor can be used to inject special test dependencies and such for unit testing of the Rpc class.
         * @param ServerRequestInterface $request
         *
         * @return HelloEndpoint
         */
        static function initFromRequest(ServerRequestInterface $request) : HelloEndpoint {
            return new HelloEndpoint();
        }


        public function hello(HelloRequest $request): HelloResponse {
            $response = new HelloResponse();
            $response->message = "Hello, {$request->name}";
            $response->helloTime = new RpcDateTime(new \DateTimeImmutable());
            return $response;
        }

        #[Retryable]
        public function goodbye(GoodbyeRequest $request): GoodbyeResponse {
            $response = new GoodbyeResponse();
            $response->message = "Goodbye {$request->person->firstName}. Don't crash your {$request->person->drives->brand} {$request->person->drives->model}!";
            return $response;
        }

        #[Retryable(5)]
        public function circular(CircularRef $circularRef): CircularRef {
            $circularRef->someValue = $circularRef->someValue + 3;
            return $circularRef;
        }
    }