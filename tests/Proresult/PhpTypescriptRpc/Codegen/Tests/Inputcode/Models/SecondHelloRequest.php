<?php
    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Models;


    class SecondHelloRequest {
        public Person $to;

        /**
         * @param Person $to
         */
        public function __construct(Person $to) {
            $this->to   = $to;
        }


    }