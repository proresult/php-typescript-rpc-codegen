<?php
    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Models;


    class GoodbyeRequest {
        public Person $person;
        /**
         * @var array<string, string>
         */
        public array $langMapping;
    }