<?php
    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Models;

    class Person {
        public string $firstName;
        public string $lastName;
        public Car $drives;

        public function __construct(
            string $firstName,
            string $lastName,
            Car $drives,
        ) {
            $this->firstName = $firstName;
            $this->lastName = $lastName;
            $this->drives = $drives;
        }

        function __toString() : string {
            return "Person: $this->firstName $this->lastName. Drives a $this->drives";
        }
    }