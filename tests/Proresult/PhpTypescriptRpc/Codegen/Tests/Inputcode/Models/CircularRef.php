<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Models;

/**
 * This model class has a circular dependency on itself.
 * The typescript code generator should then avoid adding a import for this, since that will fail to compile.
 */
class CircularRef {
    public function __construct(
        public ?CircularRef $innerRef1,
        public int $someValue,
        public CircularRef | null $innerRef2,
        /** @var array<int, CircularRef> $innerRefs3 */
        public array $innerRefs3,
    ) {
    }
}
