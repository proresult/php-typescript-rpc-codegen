<?php
    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Models;


    class Car {
        const BRAND_TESLA = "Tesla";
        const BRAND_VOLVO = "Volvo";

        public string $brand;
        public string $model;
        public int $year;

        /**
         * Car constructor.
         *
         * @param string $brand
         * @param string $model
         * @param int    $year
         */
        public function __construct(string $brand, string $model, int $year) {
            $this->brand = $brand;
            $this->model = $model;
            $this->year  = $year;
        }


        public function __toString() : string {
            return "$this->year $this->brand $this->model";
        }
    }