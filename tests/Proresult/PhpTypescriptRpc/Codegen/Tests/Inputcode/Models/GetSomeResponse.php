<?php
    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Models;

    /**
     * Response class for GetEndpoint::getSome
     */
    class GetSomeResponse {
        public string $txt1;
        public string $txt2;
        public int $num1;

        /**
         * @param string $txt1
         * @param string $txt2
         * @param int    $num1
         */
        public function __construct(string $txt1, string $txt2, int $num1) {
            $this->txt1 = $txt1;
            $this->txt2 = $txt2;
            $this->num1 = $num1;
        }
    }