<?php
    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Models;


    class UnionContainer {
        public int | string $numberOrText;
        public string | bool | null $textBoolOrNull;

    }