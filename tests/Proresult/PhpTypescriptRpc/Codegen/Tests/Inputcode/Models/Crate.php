<?php
    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Models;


    use \Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Models\Fruit\Banana;

    class Crate {
        public const MATERIAL = "wood";

        /** @var array<int, Banana> */
        public $bananas;

    }