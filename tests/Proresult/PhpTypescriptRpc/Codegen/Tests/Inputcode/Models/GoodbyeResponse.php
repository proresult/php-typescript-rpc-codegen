<?php
    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Models;


    class GoodbyeResponse {
        public string $message;
        public UnionContainer $unionContainer;
        public ?Car $maybeCar;
        public Car | null $carOrNull;
        /**
         * @var array<int, Person>
         */
        public array $greeters;
    }