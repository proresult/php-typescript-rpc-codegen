<?php
    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Models;
    use PhpParser\Node\Expr\Cast\Double;

    class Constants {
        public const A_NUMBER = 123;
        public const A_STRING = "foo";
        public const AN_ARR  = ["bar", 2, 3];
        public const OTHER_CLS_CONST = Crate::MATERIAL;
        public const DEPENDENCY_CLS_CONST = Double::KIND_REAL;
        public const ARR_CLS_CONST = ["steel", Crate::MATERIAL];
        public const A_CALC = 1 + 3;

        private const A_SECRET = "xxx"; // Should not be transpiled to typescript
    }