<?php
    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen\Tests;


    use phpDocumentor\Reflection\Fqsen;
    use phpDocumentor\Reflection\Php\Argument;
    use phpDocumentor\Reflection\Php\Class_;
    use phpDocumentor\Reflection\Php\Method;
    use phpDocumentor\Reflection\Types\Object_;
    use PHPUnit\Framework\TestCase;
    use Proresult\PhpTypescriptRpc\Codegen\Php\RpcClass;

    class RpcClassTest extends TestCase {


        private function rpcFqsen(): Fqsen {
            return new Fqsen("\\Proresult\\PhpTypescriptRpc\\Testing\\RpcClass");
        }

        private function invalidClass1(): Class_ {
            $rpcFqsen = $this->rpcFqsen();
            $class = new Class_(
                fqsen: $rpcFqsen,
            );
            $method1 = new Method( fqsen: new Fqsen($rpcFqsen->__toString()."\\someMethod"));
            $class->addMethod($method1);
            return $class;
        }

        private function validClass1(): Class_ {
            $rpcFqsen = $this->rpcFqsen();
            $class = $this->invalidClass1();
            $requestFqsen = new Fqsen(RpcClass::DEFAULT_INIT_FROM_REQUEST_ARGUMENT_TYPE);
            $responseFqsen = $rpcFqsen;

            $method2 = new Method(
                fqsen: new Fqsen($rpcFqsen->__toString()."\\initFromRequest"),
                static: true,
                returnType: new Object_($responseFqsen),
            );
            $method2->addArgument(
                new Argument(
                    name: "request",
                    type: new Object_(
                    fqsen: $requestFqsen
                ),
                )
            );
            $class->addMethod($method2);
            return $class;
        }

        private function invalidClass2(): Class_ {
            $rpcFqsen = $this->rpcFqsen();
            $class = $this->validClass1();
            // Having a handleRequest method in a rpc class is illegal
            $method3 = new Method( fqsen: new Fqsen($rpcFqsen->__toString()."\\handleRequest"));
            $class->addMethod($method3);
            return $class;
        }

        function testValidateMissingInitFromRequest() {
            $class = $this->invalidClass1();
            $rpcFqsen = $this->rpcFqsen();
            $this->expectExceptionMessage("rpc class {$rpcFqsen} does not implement the required static initFromRequest function");
            RpcClass::validate($class, null, false);
        }

        function testValidateOk() {
            $class = $this->validClass1();
            RpcClass::validate($class, null, false);
            $this->assertTrue(true); // Test ok as long as no exception is thrown
        }

        function testValidateWithHandleRequestMethod() {
            $class = $this->invalidClass2();
            $rpcFqsen = $this->rpcFqsen();
            $this->expectExceptionMessage("rpc class {$rpcFqsen} has method handleRequest. This is a reserved method name for the generated rpc adapter class.");
            RpcClass::validate($class, null, false);
        }
    }