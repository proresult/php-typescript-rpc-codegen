<?php
    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen\Tests\Exceptions;

    use Proresult\PhpTypescriptRpc\Codegen\Exceptions\ErrorHandlerException;
    use PHPUnit\Framework\TestCase;

    class ErrorHandlerExceptionTest extends TestCase {

        function testHandler() {
            ErrorHandlerException::setErrorHandler();

            try {
                // Do something that fails
                $nada = file_get_contents("/not/existing/path");
                // If we get here the error handler does not work as expected
                $this->fail("Invalid file_get_contents call did not raise expected exception");
            } catch (ErrorHandlerException $e) {
                $expectedMsg = "file_get_contents(/not/existing/path): Failed to open stream: No such file or directory";
                $this->assertEquals($expectedMsg, $e->getMessage());
            }
        }
    }
