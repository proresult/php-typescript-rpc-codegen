<?php
    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen\Tests;

    use PHPUnit\Framework\TestCase;
    use Proresult\PhpTypescriptRpc\Codegen\Codegen;
    use Proresult\PhpTypescriptRpc\Codegen\Php\AuthTypeReference;
    use Proresult\PhpTypescriptRpc\Codegen\Php\ClassReference;
    use Proresult\PhpTypescriptRpc\Codegen\Php\NamespaceAdjuster;
    use Proresult\PhpTypescriptRpc\Codegen\Php\NamespaceRep;
    use Proresult\PhpTypescriptRpc\Codegen\Php\RpcClass;
    use Proresult\PhpTypescriptRpc\Codegen\Php\RpcMethod;
    use Proresult\PhpTypescriptRpc\Codegen\Php\RpcMethodArgument;
    use Proresult\PhpTypescriptRpc\Codegen\Php\RpcMethodArguments;
    use Proresult\PhpTypescriptRpc\Codegen\Php\RpcMethods;
    use Proresult\PhpTypescriptRpc\Codegen\Php\ScalarReference;
    use Proresult\PhpTypescriptRpc\Codegen\PhpRpcAdapterCoder;

    class RpcAdapterCoderTest extends TestCase {

        public const expectedHelloRpcAdapterCode =
'<?php
    /* Code generated by php-typescript-rpc-codegen. Do not edit manually. */

    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen\Tests\Generated\RpcAdapters;

    use Proresult\PhpTypescriptRpc\Server\RequestResponseInterface;
    use Proresult\PhpTypescriptRpc\Server\RequestResponseUtils;
    use Proresult\PhpTypescriptRpc\Server\Exceptions\ForbiddenException;
    use Psr\Http\Message\ResponseInterface;
    use Psr\Http\Message\ServerRequestInterface;
    use Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Rpc\HelloRpc;
    use Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Models\HelloRequest;
    use Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Models\HelloResponse;

class HelloRpcAdapter implements RequestResponseInterface {

    private RequestResponseUtils $requestResponseUtils;
    private ?HelloRpc $rpc;

    /**
     * RequestResponseAdapter constructor.
     */
    public function __construct(RequestResponseUtils $requestResponseUtils, ?HelloRpc $rpc = null) {
        $this->requestResponseUtils = $requestResponseUtils;
        $this->rpc = $rpc;
    }

    public function hello(ServerRequestInterface $_httpRequest): ResponseInterface {
        $this->requestResponseUtils->ensureMethod($_httpRequest, "POST");
        $_requestBodyStr = $this->requestResponseUtils->getBodyStr($_httpRequest);
        $request = $this->requestResponseUtils->deserialize($_requestBodyStr, HelloRequest::class);
        if ($this->rpc === null) {
            $this->rpc = HelloRpc::initFromRequest($_httpRequest);
        }
        $_rpcResponse = $this->rpc->hello($request);
        return $this->requestResponseUtils->successfulRpcJsonResponse($_rpcResponse);
    }

    public function handleRequest(ServerRequestInterface $request): ?ResponseInterface {
        return match ($request->getUri()->getPath()) {
            "/HelloRpc/hello" => $this->hello($request),
            default => null,
        };
    }
}
';

        public const expectedGetEndpointAdapterCode =
            '<?php
    
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen\Tests\Generated\RpcAdapters;

    use Proresult\PhpTypescriptRpc\Server\RequestResponseInterface;
    use Proresult\PhpTypescriptRpc\Server\RequestResponseUtils;
    use Proresult\PhpTypescriptRpc\Server\Exceptions\ForbiddenException;
    use Psr\Http\Message\ResponseInterface;
    use Psr\Http\Message\ServerRequestInterface;
    use Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Rpc\GetRpc;
    use Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Models\GetSomeResponse;
    use Proresult\PhpTypescriptRpc\Codegen\Tests\EnrichedRequest;

class GetRpcAdapter implements RequestResponseInterface {

    private RequestResponseUtils $requestResponseUtils;
    private ?GetRpc $rpc;

    /**
     * RequestResponseAdapter constructor.
     */
    public function __construct(RequestResponseUtils $requestResponseUtils, ?GetRpc $rpc = null) {
        $this->requestResponseUtils = $requestResponseUtils;
        $this->rpc = $rpc;
    }

    public function getSome(ServerRequestInterface $_httpRequest): ResponseInterface {
        $this->requestResponseUtils->ensureMethod($_httpRequest, "GET");
        $_httpRequest = new EnrichedRequest($_httpRequest);
        $_rpcParams = $this->requestResponseUtils->getQueryParams($_httpRequest);
        $txt1 = $this->requestResponseUtils->getStringParam($_rpcParams, "txt1", null);
        $num1 = $this->requestResponseUtils->getIntParam($_rpcParams, "num1", null);
        $txt2 = $this->requestResponseUtils->getStringParam($_rpcParams, "txt2", "defaultValue");
        if ($this->rpc === null) {
            $this->rpc = GetRpc::initFromRequest($_httpRequest);
        }
        $_rpcResponse = $this->rpc->getSome(txt1: $txt1, num1: $num1, txt2: $txt2);
        return $this->requestResponseUtils->successfulRpcJsonResponse($_rpcResponse);
    }

    public function getWithoutParams(ServerRequestInterface $_httpRequest): ResponseInterface {
        $this->requestResponseUtils->ensureMethod($_httpRequest, "GET");
        $_httpRequest = new EnrichedRequest($_httpRequest);
        if ($this->rpc === null) {
            $this->rpc = GetRpc::initFromRequest($_httpRequest);
        }
        $_rpcResponse = $this->rpc->getWithoutParams();
        return $this->requestResponseUtils->successfulRpcJsonResponse($_rpcResponse);
    }

    public function getWithNullableParams(ServerRequestInterface $_httpRequest): ResponseInterface {
        $this->requestResponseUtils->ensureMethod($_httpRequest, "GET");
        $_httpRequest = new EnrichedRequest($_httpRequest);
        $_rpcParams = $this->requestResponseUtils->getQueryParams($_httpRequest);
        $txt1 = $this->requestResponseUtils->getOptionalStringParam($_rpcParams, "txt1", null);
        $num1 = $this->requestResponseUtils->getOptionalIntParam($_rpcParams, "num1", null);
        $txt2 = $this->requestResponseUtils->getOptionalStringParam($_rpcParams, "txt2", "defaultValue");
        if ($this->rpc === null) {
            $this->rpc = GetRpc::initFromRequest($_httpRequest);
        }
        $_rpcResponse = $this->rpc->getWithNullableParams(txt1: $txt1, num1: $num1, txt2: $txt2);
        return $this->requestResponseUtils->successfulRpcJsonResponse($_rpcResponse);
    }

    public function handleRequest(ServerRequestInterface $request): ?ResponseInterface {
        return match ($request->getUri()->getPath()) {
            "/GetRpc/getSome" => $this->getSome($request),
            "/GetRpc/getWithoutParams" => $this->getWithoutParams($request),
            "/GetRpc/getWithNullableParams" => $this->getWithNullableParams($request),
            default => null,
        };
    }
}
';

        public const expectedAuthenticatedRpcAdapterCode =
            '<?php
    /* Code generated by php-typescript-rpc-codegen. Do not edit manually. */

    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen\Tests\Generated\RpcAdapters;

    use Proresult\PhpTypescriptRpc\Server\RequestResponseInterface;
    use Proresult\PhpTypescriptRpc\Server\RequestResponseUtils;
    use Proresult\PhpTypescriptRpc\Server\Exceptions\ForbiddenException;
    use Psr\Http\Message\ResponseInterface;
    use Psr\Http\Message\ServerRequestInterface;
    use Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Rpc\AuthenticatedRpc;
    use Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Models\GetSomeResponse;
    use Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Models\HelloRequest;
    use Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Models\HelloResponse;
    use Proresult\PhpTypescriptRpc\Codegen\Tests\EnrichedRequest;
    use Proresult\PhpTypescriptRpc\Codegen\Tests\Authcontroller1;

class AuthenticatedRpcAdapter implements RequestResponseInterface {

    private RequestResponseUtils $requestResponseUtils;
    private ?AuthenticatedRpc $rpc;

    /**
     * RequestResponseAdapter constructor.
     */
    public function __construct(RequestResponseUtils $requestResponseUtils, ?AuthenticatedRpc $rpc = null) {
        $this->requestResponseUtils = $requestResponseUtils;
        $this->rpc = $rpc;
    }

    public function getWithoutParams(ServerRequestInterface $_httpRequest): ResponseInterface {
        $this->requestResponseUtils->ensureMethod($_httpRequest, "GET");
        $_httpRequest = new EnrichedRequest($_httpRequest);
        $_authController = new Authcontroller1($_httpRequest);
        if (!$_authController->allow()) {
            throw new ForbiddenException();
        }
        if ($this->rpc === null) {
            $this->rpc = AuthenticatedRpc::initFromRequest($_httpRequest);
        }
        $_rpcResponse = $this->rpc->getWithoutParams();
        return $this->requestResponseUtils->successfulRpcJsonResponse($_rpcResponse);
    }

    public function hello(ServerRequestInterface $_httpRequest): ResponseInterface {
        $this->requestResponseUtils->ensureMethod($_httpRequest, "POST");
        $_httpRequest = new EnrichedRequest($_httpRequest);
        $_authController = new Authcontroller1($_httpRequest);
        $_requestBodyStr = $this->requestResponseUtils->getBodyStr($_httpRequest);
        $request = $this->requestResponseUtils->deserialize($_requestBodyStr, HelloRequest::class);
        if (!$_authController->allow()) {
            throw new ForbiddenException();
        }
        if ($this->rpc === null) {
            $this->rpc = AuthenticatedRpc::initFromRequest($_httpRequest);
        }
        $_rpcResponse = $this->rpc->hello($request);
        return $this->requestResponseUtils->successfulRpcJsonResponse($_rpcResponse);
    }

    public function handleRequest(ServerRequestInterface $request): ?ResponseInterface {
        return match ($request->getUri()->getPath()) {
            "/AuthenticatedRpc/getWithoutParams" => $this->getWithoutParams($request),
            "/AuthenticatedRpc/hello" => $this->hello($request),
            default => null,
        };
    }
}
';
        private NamespaceAdjuster $adjuster;
        private NamespaceRep $baseNs;

        public function __construct(?string $name = null, array $data = [], $dataName = '') {
            parent::__construct($name, $data, $dataName);

            $this->adjuster = new NamespaceAdjuster("\\Inputcode\\Rpc", "\\Generated\\RpcAdapters");
            $this->baseNs = new NamespaceRep(["Proresult", "PhpTypescriptRpc", "Codegen", "Tests", "Inputcode", "Rpc"]);
        }


        function testPhpCode() {
            $helloMethod = new RpcMethod(
                arguments: new RpcMethodArguments([new RpcMethodArgument("request", new ClassReference("Proresult\\PhpTypescriptRpc\\Codegen\\Tests\\Inputcode\\Models", "HelloRequest"),null, false)]),
                returnType: new ClassReference("Proresult\\PhpTypescriptRpc\\Codegen\\Tests\\Inputcode\\Models", "HelloResponse"),
                rpcClassRef: new ClassReference("Proresult\\PhpTypescriptRpc\\Codegen\\Tests\\Inputcode\\Rpc", "HelloRpc"),
                methodName: "hello",
                isGetMethod: false,
                authCheck: null,
                retryable: null,
            );
            $helloRpcAdapter = new RpcClass(
                rpcClassRef: new ClassReference("Proresult\\PhpTypescriptRpc\\Codegen\\Tests\\Inputcode\\Rpc", "HelloRpc"),
                rpcMethods: new RpcMethods([$helloMethod]),
                requestClassRef: null,
                authTypeRef: null,
            );
            $getMethod = new RpcMethod(
                arguments: new RpcMethodArguments([
                    new RpcMethodArgument("txt1", new ScalarReference("string"), null, false),
                    new RpcMethodArgument("num1", new ScalarReference("int"), null, false),
                    new RpcMethodArgument("txt2", new ScalarReference("string"), '"defaultValue"', false),
                ]),
                returnType: new ClassReference("Proresult\\PhpTypescriptRpc\\Codegen\\Tests\\Inputcode\\Models", "GetSomeResponse"),
                rpcClassRef: new ClassReference("Proresult\\PhpTypescriptRpc\\Codegen\\Tests\\Inputcode\\Rpc", "GetRpc"),
                methodName: "getSome",
                isGetMethod: true,
                authCheck: null,
                retryable: null,
            );
            $getNoParamsMethod = new RpcMethod(
                arguments: new RpcMethodArguments([]),
                returnType: new ClassReference("Proresult\\PhpTypescriptRpc\\Codegen\\Tests\\Inputcode\\Models", "GetSomeResponse"),
                rpcClassRef: new ClassReference("Proresult\\PhpTypescriptRpc\\Codegen\\Tests\\Inputcode\\Rpc", "GetRpc"),
                methodName: "getWithoutParams",
                isGetMethod: true,
                authCheck: null,
                retryable: null,
            );
            $getNullableParamsMethod = new RpcMethod(
                arguments: new RpcMethodArguments([
                    new RpcMethodArgument("txt1", new ScalarReference("string"), null, true),
                    new RpcMethodArgument("num1", new ScalarReference("int"), null, true),
                    new RpcMethodArgument("txt2", new ScalarReference("string"), '"defaultValue"', true),
                ]),
                returnType: new ClassReference("Proresult\\PhpTypescriptRpc\\Codegen\\Tests\\Inputcode\\Models", "GetSomeResponse"),
                rpcClassRef: new ClassReference("Proresult\\PhpTypescriptRpc\\Codegen\\Tests\\Inputcode\\Rpc", "GetRpc"),
                methodName: "getWithNullableParams",
                isGetMethod: true,
                authCheck: null,
                retryable: null,
            );
            $getRpcAdapter = new RpcClass(
                rpcClassRef: new ClassReference("Proresult\\PhpTypescriptRpc\\Codegen\\Tests\\Inputcode\\Rpc", "GetRpc"),
                rpcMethods: new RpcMethods([$getMethod, $getNoParamsMethod, $getNullableParamsMethod]),
                requestClassRef: new ClassReference("Proresult\\PhpTypescriptRpc\\Codegen\\Tests", "EnrichedRequest"),
                authTypeRef: null,
            );
            $authClassRef = new ClassReference("Proresult\\PhpTypescriptRpc\\Codegen\\Tests", "Authcontroller1");
            $defaultMethodName = "allow";
            $authTypeRef = new AuthTypeReference($authClassRef, $defaultMethodName);

            $coder = new PhpRpcAdapterCoder($this->baseNs, $this->adjuster, $helloRpcAdapter, Codegen::DEFAULT_PHP_HEADER_COMMENT);
            $this->assertEquals(self::expectedHelloRpcAdapterCode, $coder->phpCode());
            $coder = new PhpRpcAdapterCoder($this->baseNs, $this->adjuster, $getRpcAdapter, "");
            $this->assertEquals(self::expectedGetEndpointAdapterCode, $coder->phpCode());

            // AuthenticatedRpc use a combination of methods from the other to classes
            $helloMethod->rpcClassRef->name = "AuthenticatedRpc";
            $getNoParamsMethod->rpcClassRef->name = "AuthenticatedRpc";
            $authenticatedRpcAdapter = new RpcClass(
                rpcClassRef: $getNoParamsMethod->rpcClassRef,
                rpcMethods: new RpcMethods([$getNoParamsMethod, $helloMethod]),
                requestClassRef: $getRpcAdapter->requestClassRef,
                authTypeRef: $authTypeRef,
            );
            $coder = new PhpRpcAdapterCoder($this->baseNs, $this->adjuster, $authenticatedRpcAdapter,Codegen::DEFAULT_PHP_HEADER_COMMENT);
            $this->assertEquals(self::expectedAuthenticatedRpcAdapterCode, $coder->phpCode());
        }

        private function getRpcClassRef(): ClassReference {
            return new ClassReference( "Proresult\\PhpTypescriptRpc\\Codegen\\Tests\\Inputcode\\Rpc", "GetRpc");
        }

        private function authcontroller1ClassRef(): ClassReference {
            return new ClassReference(
                "Proresult\\PhpTypescriptRpc\\Codegen\\Tests",
                "Authcontroller1"
            );
        }
        private function getRpcMethod(ClassReference $rpcClassRef): RpcMethod {
            return new RpcMethod(
                arguments: new RpcMethodArguments([]),
                returnType: new ClassReference(
                    "Proresult\\PhpTypescriptRpc\\Codegen\\Tests\\Inputcode\\Models",
                    "GetSomeResponse"
                ),
                rpcClassRef: $rpcClassRef,
                methodName: "getWithoutParams",
                isGetMethod: true,
                authCheck: null,
                retryable: null,
            );
        }
        private function rpcAdapter(ClassReference $rpcClassRef, RpcMethod $rpcMethod, AuthTypeReference $authTypeReference): RpcClass {
            return new RpcClass(
                rpcClassRef: $rpcClassRef,
                rpcMethods: new RpcMethods([$rpcMethod]),
                requestClassRef: new ClassReference("Proresult\\PhpTypescriptRpc\\Codegen\\Tests", "EnrichedRequest"),
                authTypeRef: $authTypeReference,
            );
        }

        function testMissingAuthCheckException() {
            $rpcClassRef = $this->getRpcClassRef();
            $authClassRef               = $this->authcontroller1ClassRef();
            $authTypeRef                = new AuthTypeReference($authClassRef, null /* NO DEFAULT AUTHENTICATION */);
            $rpcMethodWithoutAuthCheck  = $this->getRpcMethod($rpcClassRef);
            $rpcAdapterMissingAuthCheck = $this->rpcAdapter($rpcClassRef, $rpcMethodWithoutAuthCheck, $authTypeRef);
            $coder = new PhpRpcAdapterCoder(
                $this->baseNs,
                $this->adjuster,
                $rpcAdapterMissingAuthCheck,
                "",
            );
            $this->expectExceptionMessageMatches('/'.preg_quote("No auth method name provided via #AuthType or #AuthCheck attributes for method").'/');
            $coder->phpCode();
        }
    }