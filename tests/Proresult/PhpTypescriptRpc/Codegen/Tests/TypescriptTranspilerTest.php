<?php
    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen\Tests;


    use Exception;
    use phpDocumentor\Reflection\File\LocalFile;
    use phpDocumentor\Reflection\Php\ProjectFactory;
    use PHPUnit\Framework\TestCase;
    use Proresult\PhpTypescriptRpc\Codegen\Exceptions\ErrorHandlerException;
    use Proresult\PhpTypescriptRpc\Codegen\Files;
    use Proresult\PhpTypescriptRpc\Codegen\Php\NamespaceRep;
    use Proresult\PhpTypescriptRpc\Codegen\Php\RpcMethod;
    use Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Models\Car;
    use Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Models\GoodbyeRequest;
    use Proresult\PhpTypescriptRpc\Codegen\Tests\Inputcode\Models\Person;
    use Proresult\PhpTypescriptRpc\Codegen\Tests\Mock\MockArgument;
    use Proresult\PhpTypescriptRpc\Codegen\Tests\Mock\MockMethod;
    use Proresult\PhpTypescriptRpc\Codegen\Tests\Mock\MockProperty;
    use Proresult\PhpTypescriptRpc\Codegen\Typescript\Argument;
    use Proresult\PhpTypescriptRpc\Codegen\Typescript\Constant;
    use Proresult\PhpTypescriptRpc\Codegen\Typescript\File;
    use Proresult\PhpTypescriptRpc\Codegen\Typescript\Import;
    use Proresult\PhpTypescriptRpc\Codegen\Typescript\ImportSet;
    use Proresult\PhpTypescriptRpc\Codegen\Typescript\Method;
    use Proresult\PhpTypescriptRpc\Codegen\Typescript\ModulePath;
    use Proresult\PhpTypescriptRpc\Codegen\Typescript\ModulePathPrefixAdjustment;
    use Proresult\PhpTypescriptRpc\Codegen\Typescript\ModulePathPrefixAdjustments;
    use Proresult\PhpTypescriptRpc\Codegen\Typescript\Name;
    use Proresult\PhpTypescriptRpc\Codegen\Typescript\Property;
    use Proresult\PhpTypescriptRpc\Codegen\Typescript\TypescriptTypes;
    use Proresult\PhpTypescriptRpc\Codegen\TypescriptModelTranspiler;
    use Proresult\PhpTypescriptRpc\Codegen\TypescriptRpcTranspiler;
    use Proresult\PhpTypescriptRpc\Server\Attributes\Retryable;
    use Proresult\PhpTypescriptRpc\Server\RequestResponseUtils;
    use function PHPUnit\Framework\assertEquals;
    use function PHPUnit\Framework\assertTrue;

    class TypescriptTranspilerTest extends TestCase {

        protected function setUp() : void {
            parent::setUp();
            ErrorHandlerException::setErrorHandler();
        }

        public const expectedFiles = [
            "Car.php",
            "Person.php",
            "Crate.php",
            "Banana.php",
        ];

        /**
         * @var \SplFileInfo[]
         */
        private array $foundFiles = [];


        private string $inputDir = __DIR__.'/Inputcode';
        private string $modelsBaseDir;
        private ProjectFactory $projectFactory;
        private string $rpcBaseDir = __DIR__.'/Inputcode/Rpc';

        private ModulePathPrefixAdjustments $modulePathPrefixAdjustment;

        public function __construct() {
            $this->modelsBaseDir = $this->inputDir.'/Models';
            $this->projectFactory = ProjectFactory::createInstance();
            ErrorHandlerException::setErrorHandler();
            $this->modulePathPrefixAdjustment = new ModulePathPrefixAdjustments(
                new ModulePathPrefixAdjustment(
                    new ModulePath(Name::arrayFromStrings("Proresult", "PhpTypescriptRpc", "Codegen", "Tests", "Inputcode")),
                    new ModulePath(Name::arrayFromStrings("rpc", "tests" )),
                ),
                new ModulePathPrefixAdjustment(
                    new ModulePath(Name::arrayFromStrings("Proresult", "PhpTypescriptRpc", "Server", "Models", "RpcDateTime")),
                    new ModulePath(Name::arrayFromStrings("@proresult", "php-typescript-rpc"), isGenerated: false),
                )
            );
            parent::__construct();
        }


        public static function checkProperty(MockProperty $expectedProperty, Property $actualProperty) {
            assertEquals($expectedProperty->name(), $actualProperty->name());
            assertEquals($expectedProperty->getTypescriptTypes(), $actualProperty->getTypescriptTypes());
        }

        public static function checkProperties(array $expectedProps, array $actualProps) {
            assertEquals(count($expectedProps), count($actualProps));
            for($i = 0; $i < count($expectedProps); $i++) {
                self::checkProperty($expectedProps[$i], $actualProps[$i]);
            }
        }

        public static function checkArgument(MockArgument $expectedArgument, Argument $actualArgument) {
            assertEquals($expectedArgument->name(), $actualArgument->name());
            assertEquals($expectedArgument->getTypescriptTypes(), $actualArgument->getTypescriptTypes());
        }

        public static function checkArguments(array $expectedArguments, array $actualArguments) {
            assertEquals(count($expectedArguments), count($actualArguments));
            for($i = 0; $i < count($expectedArguments); $i++) {
                self::checkArgument($expectedArguments[$i], $actualArguments[$i]);
            }
        }

        public static function checkMethod(MockMethod $expectedMethod, Method $actualMethod) {
            assertEquals($expectedMethod->name(), $actualMethod->name());
            self::checkArguments($expectedMethod->getTypescriptArguments(), $actualMethod->getTypescriptArguments());
        }

        public static function checkMethods(array $expectedMethods, array $actualMethods) {
            assertEquals(count($expectedMethods), count($actualMethods));
            for($i = 0; $i < count($expectedMethods); $i++) {
                self::checkMethod($expectedMethods[$i], $actualMethods[$i]);
            }
        }

        public static function checkImportSets(ImportSet $expected, ImportSet $actual) {
            $expImps = $expected->getImports();
            $actImps = $actual->getImports();
            assertEquals(count($expImps), count($actImps));
            foreach($expImps as $expImp) {
                $found = false;
                foreach ($actImps as $actImp) {
                    if($actImp->typescript() === $expImp->typescript()) {
                        $found = true;
                    }
                }
                if(!$found) {
                    assertTrue(false,"ImportSets not equal");
                }
            }
        }

        public function testPersonParse() {
            $projectFiles = [new LocalFile($this->modelsBaseDir."/Person.php")];
            $project = $this->projectFactory->create("TestProject", $projectFiles);
            $this->assertEquals(1, count($project->getFiles()));
            $parsefile1 = array_values($project->getFiles())[0];
            $typescriptFile = new File($parsefile1, ModulePathPrefixAdjustments::none(), "");
            $this->assertEquals($this->modelsBaseDir."/Person.php", $parsefile1->getPath());
            $basePath = new ModulePath(Name::arrayFromStrings("Proresult", "PhpTypescriptRpc", "Codegen", "Tests", "Inputcode", "Models"));
            $this->assertEquals($basePath->appendName(new Name("Person")), $typescriptFile->typescriptModulePath());
            $cls = $typescriptFile->clazz();
            $carImport = new Import($basePath->appendName(new Name("Car")), new Name("Car"));
            $carDeserializeImport = new Import($basePath->appendName(new Name("Car")), new Name("deserializeCar"));
            $this->assertEquals([$carImport, $carDeserializeImport], $cls->getPropertyImportSet()->getImports());
            $actualPops = $cls->getProperties();
            $expectedProps = [
                new MockProperty("firstName", new TypescriptTypes("string")),
                new MockProperty("lastName", new TypescriptTypes("string")),
                new MockProperty("drives", new TypescriptTypes("Car", ImportSet::init($carImport, $carDeserializeImport))),
            ];
            self::checkProperties($expectedProps, $actualPops);
        }

        public const npmprojectPath = __DIR__.'/../../../../npmproject';

        public function testTranspiling(): void {
            $outputDir = self::npmprojectPath.'/src/generated'; // sys_get_temp_dir()."/php-typescript-rpc/test";
            // Clear output dir before running test
            Files::rmdirRecursive($outputDir, "ts");
            $modelTranspiler = new TypescriptModelTranspiler($this->modelsBaseDir, $outputDir, $this->modulePathPrefixAdjustment);
            $modelTranspiler->run();
            $expectedFilecount = count($modelTranspiler->getInputFiles());
            $transpiledFiles = $modelTranspiler->getTranspiledFiles();
            $this->assertEquals($expectedFilecount, count($transpiledFiles));

            $baseRpcNamespace = new NamespaceRep(["Proresult", "PhpTypescriptRpc", "Codegen", "Tests", "Inputcode", "Rpc"]);
            $rpcTranspiler = new TypescriptRpcTranspiler($this->rpcBaseDir, $outputDir, $this->modulePathPrefixAdjustment, $baseRpcNamespace);
            $rpcTranspiler->run();
            $expectedFilecount = count($rpcTranspiler->getInputFiles());
            $transpiledFiles = $rpcTranspiler->getTranspiledFiles();
            $this->assertEquals($expectedFilecount, count($transpiledFiles));
        }

        public function testHelloEndpointParse() {
            $filepath = $this->rpcBaseDir.'/HelloEndpoint.php';
            $projectFiles = [new LocalFile($filepath)];
            $project = $this->projectFactory->create("Testproject", $projectFiles);
            $this->assertEquals(1, count($project->getFiles()));
            $parsefile1 = array_values($project->getFiles())[0];
            $headerContent = "// some header content".PHP_EOL;
            $typescriptFile = new File($parsefile1, $this->modulePathPrefixAdjustment, $headerContent);
            $this->assertEquals($filepath, $parsefile1->getPath());
            $basePath = new ModulePath(Name::arrayFromStrings("Proresult", "PhpTypescriptRpc", "Codegen", "Tests", "Inputcode", "Rpc"));
            $this->assertEquals($this->modulePathPrefixAdjustment->adjust($basePath)->appendName(new Name("HelloEndpoint")), $typescriptFile->typescriptModulePath());
            $cls = $typescriptFile->clazz();
            $modelsPath = $basePath->withoutLastName()->appendName(new Name("Models"));
            $reqImport1 = new Import($modelsPath->appendName(new Name("HelloRequest")), new Name("HelloRequest"));
            $respImport1 = new Import($modelsPath->appendName(new Name("HelloResponse")), new Name("HelloResponse"));
            $reqImport2 = new Import($modelsPath->appendName(new Name("GoodbyeRequest")), new Name("GoodbyeRequest"));
            $respImport2 = new Import($modelsPath->appendName(new Name("GoodbyeResponse")), new Name("GoodbyeResponse"));
            $reqRespImport3 = new Import($modelsPath->appendName(new Name("CircularRef")), new Name("CircularRef"));
            $reqImport1Deser = new Import($modelsPath->appendName(new Name("HelloRequest")), new Name("deserializeHelloRequest"));
            $respImport1Deser = new Import($modelsPath->appendName(new Name("HelloResponse")), new Name("deserializeHelloResponse"));
            $reqImport2Deser = new Import($modelsPath->appendName(new Name("GoodbyeRequest")), new Name("deserializeGoodbyeRequest"));
            $respImport2Deser = new Import($modelsPath->appendName(new Name("GoodbyeResponse")), new Name("deserializeGoodbyeResponse"));
            $reqRespImport3Deser = new Import($modelsPath->appendName(new Name("CircularRef")), new Name("deserializeCircularRef"));
            self::checkImportSets(ImportSet::init($respImport1, $reqImport1, $reqImport2, $respImport2, $reqRespImport3, $reqImport1Deser, $respImport1Deser, $reqImport2Deser, $respImport2Deser, $reqRespImport3Deser), $cls->getMethodsImportSet());
            $actualMethods = $cls->getMethods();
            $expectedMethods = [
                new MockMethod("hello", [new MockArgument("request", new TypescriptTypes("HelloRequest", ImportSet::init($reqImport1, $reqImport1Deser)))]),
                new MockMethod("goodbye", [new MockArgument("request", new TypescriptTypes("GoodbyeRequest", ImportSet::init($reqImport2, $reqImport2Deser)))]),
                new MockMethod("circular", [new MockArgument("circularRef", new TypescriptTypes("CircularRef", ImportSet::init($reqRespImport3, $reqRespImport3Deser)))]),
            ];
            self::checkMethods($expectedMethods, $actualMethods);
            $baseRpcNamespace = new NamespaceRep(["Proresult", "PhpTypescriptRpc", "Tests", "Inputcode", "Rpc"]);
        }

        public function testInvalidUnion() {
            $projectFiles = [new LocalFile($this->inputDir."/Invalid/InvalidUnionContainer.php")];
            $project = $this->projectFactory->create("TestProject", $projectFiles);
            $this->assertEquals(1, count($project->getFiles()));
            $parsefile1 = array_values($project->getFiles())[0];
            $typescriptFile = new File($parsefile1, ModulePathPrefixAdjustments::none(), "");
            $cls = $typescriptFile->clazz();
            
            $this->expectExceptionMessage("Compound type \"\\Proresult\\PhpTypescriptRpc\\Codegen\\Tests\\Inputcode\\Models\\Car|\\Proresult\\PhpTypescriptRpc\\Codegen\\Tests\\Inputcode\\Models\\Person\" is not supported for use in PHP Typescript RPC. Must be all native json types or an object and a null");
            $cls->typescriptModelInterface(); // Should fail
        }

        public function testRequestDeSerialization() {
            $gbReq = new GoodbyeRequest();
            $gbReq->person = new Person("Knut", "Knalle", new Car(brand: "Tesla", model: "Y", year: 2021));
            $gbReq->langMapping = ["no" => "Hada", "en" => "Goodbye", "de" => "Auf wiedersehen"];
            $json = json_encode($gbReq, JSON_THROW_ON_ERROR);
            $requestResponseUtils = new RequestResponseUtils(new TestResponseFactory());
            $deserialized = $requestResponseUtils->deserialize($json, GoodbyeRequest::class);
            $this->assertEquals($gbReq, $deserialized);
        }

        public function testConstants() {
            $projectFiles = [new LocalFile($this->modelsBaseDir."/Constants.php")];
            $project = $this->projectFactory->create("TestProject", $projectFiles);
            $parsefile1 = array_values($project->getFiles())[0];
            $typescriptFile = new File($parsefile1, ModulePathPrefixAdjustments::none(), "");
            $basePath = new ModulePath(Name::arrayFromStrings("Proresult", "PhpTypescriptRpc", "Codegen", "Tests", "Inputcode", "Models"));
            $this->assertEquals($basePath->appendName(new Name("Constants")), $typescriptFile->typescriptModulePath());
            $cls = $typescriptFile->clazz();
            $expected = [
                new Constant("A_NUMBER", 123),
                new Constant("A_STRING", "foo"),
                new Constant("AN_ARR", ["bar", 2, 3]),
                new Constant("OTHER_CLS_CONST", "wood"),
                new Constant("DEPENDENCY_CLS_CONST", 3),
                new Constant("ARR_CLS_CONST", ["steel", "wood"]),
                new Constant("A_CALC", 4),
            ];
            $constants = $cls->getConstants();
            $this->assertEquals($expected, $constants);
            $expectedTs = [
                'export const A_NUMBER = 123',
                'export const A_STRING = "foo"',
                'export const AN_ARR = ["bar", 2, 3]',
                'export const OTHER_CLS_CONST = "wood"',
                'export const DEPENDENCY_CLS_CONST = 3',
                'export const ARR_CLS_CONST = ["steel", "wood"]',
                'export const A_CALC = 4',
            ];
            $this->assertEquals($expectedTs, array_map(fn(Constant $c) => $c->typescript() ,$constants));
        }

        public function testRetryable() {
            // For some unknown reason phpunit dataProvider functionality does not work here, so do this instead:
            $testData = [
                $this->rpcBaseDir.'/HelloEndpoint.php' => [ "hello" => null, "goodbye" => new Retryable(), "circular" => new Retryable(5) ],
                $this->rpcBaseDir.'/GetEndpoint.php' => [ "getSome" => new Retryable(), "getWithoutParams" => new Retryable(), "getWithOptionalParam" => new Retryable(0), "getWithNullableParam" => new Retryable() ]
            ];
            foreach ($testData as $filepath => $expectedMappings) {
                // See that parsing of HelloEndpoint works
                $projectFiles = [new LocalFile($filepath)];
                $project      = $this->projectFactory->create("Testproject", $projectFiles);
                $this->assertEquals(1, count($project->getFiles()));
                $parsefile1     = array_values($project->getFiles())[0];
                $typescriptFile = new File($parsefile1, $this->modulePathPrefixAdjustment, "");
                $cls            = $typescriptFile->clazz();
                $tsMethods      = $cls->getMethods();
                foreach ($tsMethods as $tsMethod) {
                    $rpcMethod = RpcMethod::fromMethod($tsMethod->getMethod(), $typescriptFile->getAttributeHelper());
                    $retryable = $rpcMethod->retryable;
                    if (array_key_exists($rpcMethod->methodName, $expectedMappings)) {
                        $this->assertEquals($expectedMappings[$rpcMethod->methodName], $retryable);
                    } else {
                        $this->fail("Expected mapping for rpc \"{$rpcMethod->rpcClassRef->name}\" method \"{$rpcMethod->methodName}\" not found. Add it to the test");
                    }
                }
            }

        }
    }