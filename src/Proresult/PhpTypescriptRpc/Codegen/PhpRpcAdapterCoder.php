<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen;

    use Exception;
    use Proresult\PhpTypescriptRpc\Codegen\Php\NamespaceAdjuster;
    use Proresult\PhpTypescriptRpc\Codegen\Php\NamespaceRep;
    use Proresult\PhpTypescriptRpc\Codegen\Php\RpcClass;
    use Proresult\PhpTypescriptRpc\Codegen\Php\RpcMethod;
    use Proresult\PhpTypescriptRpc\Codegen\Php\RpcMethodArgument;
    use Proresult\PhpTypescriptRpc\Codegen\Php\RpcMethodArguments;
    use Proresult\PhpTypescriptRpc\Server\Attributes\AuthCheck;

    /** Make the RpcAdapter class php code from a given RpcAdapter instance */
class PhpRpcAdapterCoder {
    private NamespaceAdjuster $namespaceAdjuster;
    private RpcClass $rpc;
    public NamespaceRep $baseNamespace;
    private string $lineSep = "\n"; // We always use unix line endings for generated code for now. Consider making it configurable later.

    public string $vnRequest = "_httpRequest";
    public string $vnRequestBodyStr = "_requestBodyStr";
    public string $vnResponse = "_rpcResponse";
    public string $vnRpcParams = "_rpcParams";
    public string $vnAuthController = "_authController";
    /** @var array<int, string> */
    public array $reservedVariableNames = [];
    public string $fileHeaderContent = "";


    /**
     * PhpRpcAdapterCoder constructor.
     *
     * @param RpcClass $rpc
     */
    public function __construct(NamespaceRep $baseNamespace, NamespaceAdjuster $namespaceAdjuster, RpcClass $rpc, string $fileHeaderContent) {
        $this->baseNamespace = $baseNamespace;
        $this->namespaceAdjuster = $namespaceAdjuster;
        $this->rpc              = $rpc;
        $this->reservedVariableNames = [$this->vnRequest, $this->vnRequestBodyStr, $this->vnResponse, $this->vnRpcParams, $this->vnAuthController];
        $this->fileHeaderContent = $fileHeaderContent;
    }

    private function checkVariableName(string $vn): void {
        if (in_array($vn, $this->reservedVariableNames)) {
            throw new Exception(
                "\"{$vn}\" is a reserved variable name and must therefore not be used as argument name in rpc methods. " .
                "Reserved names are: " . implode(", ", $this->reservedVariableNames)
            );
        }
    }

    // XXX Should ideally rewrite all generator code to make some line objects instead, to simplify proper indentation and removal of empty lines.
    private function removeEmptyLines(string $code): string {
        $lines = explode($this->lineSep, $code);
        $lines = array_filter($lines, fn(string $line) => strlen(trim($line)) > 0);
        return implode($this->lineSep, $lines);
    }

    private function phpCodeRequestTypeConstructCall(): string {
        if ($this->rpc->requestClassRef !== null) {
            return Formatter::indentSpaces(8) . "\${$this->vnRequest} = new {$this->rpc->requestClassRef->name}(\${$this->vnRequest});" . $this->lineSep;
        } else {
            return "";
        }
    }

    private function phpCodeRequestTypeUse(): string {
        if ($this->rpc->requestClassRef !== null) {
            return Formatter::indentSpaces(4) . "use {$this->rpc->requestClassRef->__toString()};";
        } else {
            return "";
        }
    }

    private function phpCodeAuthTypeConstructCall(): string {
        if ($this->rpc->authTypeRef !== null) {
            return Formatter::indentSpaces(8) .
                   "\${$this->vnAuthController} = new {$this->rpc->authTypeRef->controllerClass->name}(\${$this->vnRequest});" .
                   $this->lineSep;
        } else {
            return "";
        }
    }

    private function phpCodeAuthTypeUse(): string {
        if ($this->rpc->authTypeRef !== null) {
            $controllerClass = $this->rpc->authTypeRef->controllerClass->__toString();
            $ret = Formatter::indentSpaces(4) .  "use {$controllerClass};";
            return $ret;
        } else {
            return "";
        }
    }

    private function phpCodeAuthCall(RpcMethod $method): string {
        if ($this->rpc->authTypeRef !== null) { // #AuthType attribute is set on rpc class.
            $authCheck = $method->authCheck;
            // If #AuthCheck don't have method set (empty attribute), use the rpc method name.
            if ($authCheck !== null && $authCheck->methodName === null) {
                $authCheck->methodName = $method->methodName;
            }
            // If no #AuthCheck is set, use the #AuthType default method, with no params.
            if ($authCheck === null) {
                $authCheck = new AuthCheck(
                    $this->rpc->authTypeRef->defaultMethodName,
                    false /* don't include params when calling default method */
                );
            }
            if (empty($authCheck->methodName)) {
                // No auth method provided via #AuthCheck or default method on #AuthType.
                throw new Exception(
                    "No auth method name provided via #AuthType or #AuthCheck attributes for method " .
                    "{$method->rpcClassRef->getFqsen()} {$method->methodName}"
                );
            }
            if (!$authCheck->includeParams && count($authCheck->paramMaps) > 0) {
                // Param map should not be provided when no params are to be included in auth check
                throw new Exception(
                    "Param map provided, but #AuthCheck not set to include parameters for method " .
                    "{$method->rpcClassRef->getFqsen()} {$method->methodName}"
                );
            }
            $args = "";
            if ($authCheck->includeParams) {
                // Use param map if provided, original method signature if not.
                if (count($authCheck->paramMaps) > 0) {
                    $args = join(", ", array_map(fn(string $paramMap) => '$' . $paramMap, $authCheck->paramMaps));
                } else {
                    if ($method->isPostMethod()) {
                        $args = '$' . $method->requestBodyArgument()->paramName;
                    } else {
                        $args = join(", ", array_map(fn(RpcMethodArgument $argument) => '$' . $argument->paramName, $method->arguments->arguments));
                    }
                }
            }
            return Formatter::indentSpaces(8) . "if (!\${$this->vnAuthController}->{$authCheck->methodName}($args)) {" . $this->lineSep .
                   Formatter::indentSpaces(12) . "throw new ForbiddenException();" . $this->lineSep .
                   Formatter::indentSpaces(8) . "}" . $this->lineSep;
        } elseif ($method->authCheck !== null) {
            // #AuthCheck attribute set without having #AuthType set.
            throw new Exception(
                "#AuthCheck attribute found on method {$method->rpcClassRef->getFqsen()}::{$method->methodName}, " .
                "but no #AuthType set on rpc class."
            );
        } else {
            return "";
        }
    }

    private function phpCodeRpcInit(): string {
        return "if (\$this->rpc === null) {" . $this->lineSep .
        "            \$this->rpc = {$this->rpc->className()}::initFromRequest(\${$this->vnRequest});
        }";
    }

    /**
     * Generate the php rpc adapter code for a given POST RpcMethod
     * @param RpcMethod $method A instace with isGetMethod === false (POST method rpc)
     *
     * @return string
     */
    private function phpCodePostRpcMethod(RpcMethod $method): string {
        if ($method->isGetMethod()) {
            throw new Exception("{$method->methodName} is a GET method. Use phpCodeGetRpcMethod to generate adapter code for it.");
        }
        $requestBodyClassReference = $method->requestBodyArgument()->getClassReference()->name;
        $requestBodyArgumentName = $method->requestBodyArgument()->paramName;
        $this->checkVariableName($requestBodyArgumentName);
        $code = "
    public function {$method->methodName}(ServerRequestInterface \${$this->vnRequest}): ResponseInterface {
        \$this->requestResponseUtils->ensureMethod(\${$this->vnRequest}, \"POST\");
{$this->phpCodeRequestTypeConstructCall()}
{$this->phpCodeAuthTypeConstructCall()}
        \${$this->vnRequestBodyStr} = \$this->requestResponseUtils->getBodyStr(\${$this->vnRequest});
        \${$requestBodyArgumentName} = \$this->requestResponseUtils->deserialize(\${$this->vnRequestBodyStr}, {$requestBodyClassReference}::class);
{$this->phpCodeAuthCall($method)}
        {$this->phpCodeRpcInit()}
        \${$this->vnResponse} = \$this->rpc->{$method->methodName}(\${$requestBodyArgumentName});
        return \$this->requestResponseUtils->successfulRpcJsonResponse(\${$this->vnResponse});
    }
";
        return $this->removeEmptyLines($code);
    }

    /**
     * @param RpcMethodArguments $args
     *
     * @return array<string, string>
     * @throws Exception
     */
    private function getRpcMethodArguments(RpcMethodArguments $args): array {
        /**
         * @var array<string, string>
         */
        $ret = [];
        foreach ($args->arguments as $arg) {
            if ($arg->isScalarReference()) {
                $defaultValue = $arg->defaultValue !== null ? $arg->defaultValue : "null";
                if ($arg->isNullable()) { // Nullable query arguments is handled differently from non-nullable
                    $getParamMethodName   = match ($arg->getScalarReference()->typeName) {
                        "string" => "getOptionalStringParam",
                        "int" => "getOptionalIntParam",
                        "float" => "getOptionalFloatParam",
                        "bool" => "getOptionalBoolParam",
                    };
                    $getCall = "\$this->requestResponseUtils->{$getParamMethodName}(\${$this->vnRpcParams}, \"{$arg->paramName}\", {$defaultValue});";
                    $ret[$arg->paramName] = $getCall;
                } else {
                    $getParamMethodName   = match ($arg->getScalarReference()->typeName) {
                        "string" => "getStringParam",
                        "int" => "getIntParam",
                        "float" => "getFloatParam",
                        "bool" => "getBoolParam",
                    };
                    $getCall = "\$this->requestResponseUtils->{$getParamMethodName}(\${$this->vnRpcParams}, \"{$arg->paramName}\", {$defaultValue});";
                    $ret[$arg->paramName] = $getCall;
                }
            } else {
                throw new Exception("GET method argument {$arg->paramName} is not a ScalarReference. Other types are not supported for GET methods yet.");
            }
        }
        return $ret;
    }


    private function phpCodeGetRpcMethod(RpcMethod $method): string {
        if (!$method->isGetMethod()) {
            throw new Exception("{$method->methodName} is NOT a GET method. Use phpCodePostRpcMethod to generate adapter code for it.");
        }
        $paramGetValueLines = [];
        $paramCallLines = [];
        foreach ($this->getRpcMethodArguments($method->arguments) as $paramName => $getCall) {
            $this->checkVariableName($paramName);
            $paramGetValueLines[] = "\${$paramName} = $getCall";
            $paramCallLines[] = "{$paramName}: \${$paramName}";
        }
        $paramGetCode = "";
        if (!empty($paramGetValueLines)) {
            $paramGetCode = Formatter::indentSpaces(8); // indent before start of line
            $paramGetCode .= "\${$this->vnRpcParams} = \$this->requestResponseUtils->getQueryParams(\${$this->vnRequest});" . $this->lineSep;
            $paramGetCode .= Formatter::indentSpaces(8); // Indent before start of line
            $paramGetCode .= implode($this->lineSep . Formatter::indentSpaces(8), $paramGetValueLines);
        }
        $paramCallCode = implode(", ", $paramCallLines);

        $code = "
    public function {$method->methodName}(ServerRequestInterface \${$this->vnRequest}): ResponseInterface {
        \$this->requestResponseUtils->ensureMethod(\${$this->vnRequest}, \"GET\");
{$this->phpCodeRequestTypeConstructCall()}
{$this->phpCodeAuthTypeConstructCall()}
{$paramGetCode}
{$this->phpCodeAuthCall($method)}
        {$this->phpCodeRpcInit()}
        \${$this->vnResponse} = \$this->rpc->{$method->methodName}({$paramCallCode});
        return \$this->requestResponseUtils->successfulRpcJsonResponse(\${$this->vnResponse});
    }
";
        return $this->removeEmptyLines($code);
    }

    private function phpCodeRpcMethod(RpcMethod $method): string {
        if ($method->isGetMethod()) {
            return $this->phpCodeGetRpcMethod($method);
        } else {
            return $this->phpCodePostRpcMethod($method);
        }
    }

    private function phpCodeRpcMethods(): string {
        $code = array_map(fn(RpcMethod $method) => $this->phpCodeRpcMethod($method), $this->rpc->rpcMethods->methods);
        return join($this->lineSep . $this->lineSep, $code);
    }

    private function phpCodeRpcMethodsUse(): string {
        $uses = $this->rpc->rpcMethods->getUses();
        $code = array_map(fn(string $namespace) => Formatter::indentSpaces(4) . "use $namespace;", $uses);
        return join($this->lineSep, $code);
    }

    private function phpCodeRpcMethodRequestMatch(RpcMethod $method): string {
        $path = $method->rpcPath($this->baseNamespace);
        return Formatter::indentSpaces(12) . "\"{$path}\" => \$this->{$method->methodName}(\$request),";
    }

    private function phpCodeRpcMethodsRequestMatch(): string {
        $code = array_map(fn(RpcMethod $method) => $this->phpCodeRpcMethodRequestMatch($method), $this->rpc->rpcMethods->methods);
        return join($this->lineSep, $code);
    }

    private function rpcAdapterNamespace(): string {
        return $this->namespaceAdjuster->adjust($this->rpc->rpcClassRef->namespace);
    }

    public function adapterClassName(): string {
        return $this->rpc->className() . "Adapter";
    }

    /**
     * @return string All the "dynamic" use statements to output. Should avoid unneccessary empty lines.
     */
    private function phpCodeUse(): string {
        $use = $this->phpCodeRpcMethodsUse();
        $enricherUse = $this->phpCodeRequestTypeUse();
        if ($enricherUse !== "") {
            $use .= $this->lineSep;
            $use .= $enricherUse;
        }
        $authUse = $this->phpCodeAuthTypeUse();
        if ($authUse !== "") {
            $use .= $this->lineSep;
            $use .= $authUse;
        }
        return $use;
    }

    public function phpCode(): string {
        return
        "<?php
    {$this->fileHeaderContent}
    declare(strict_types=1);

    namespace {$this->rpcAdapterNamespace()};

    use Proresult\PhpTypescriptRpc\Server\RequestResponseInterface;
    use Proresult\PhpTypescriptRpc\Server\RequestResponseUtils;
    use Proresult\PhpTypescriptRpc\Server\Exceptions\ForbiddenException;
    use Psr\Http\Message\ResponseInterface;
    use Psr\Http\Message\ServerRequestInterface;
    use {$this->rpc->rpcClassRef};
{$this->phpCodeUse()}

class {$this->adapterClassName()} implements RequestResponseInterface {

    private RequestResponseUtils \$requestResponseUtils;
    private ?{$this->rpc->className()} \$rpc;

    /**
     * RequestResponseAdapter constructor.
     */
    public function __construct(RequestResponseUtils \$requestResponseUtils, ?{$this->rpc->className()} \$rpc = null) {
        \$this->requestResponseUtils = \$requestResponseUtils;
        \$this->rpc = \$rpc;
    }

{$this->phpCodeRpcMethods()}

    public function handleRequest(ServerRequestInterface \$request): ?ResponseInterface {
        return match (\$request->getUri()->getPath()) {
{$this->phpCodeRpcMethodsRequestMatch()}
            default => null,
        };
    }
}
";
    }
}
