<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Codegen\PhpParser;

use Exception;
use PhpParser\Node;
use PhpParser\Node\Stmt\Class_;
use PhpParser\NodeTraverser;

class SingleClassExtractingNodeVisitor extends \PhpParser\NodeVisitorAbstract {
    private string $fileName;
    private Class_|null $cls = null;
    /**
     * @param string $fileName
     */
    public function __construct(string $fileName) {
        $this->fileName = $fileName;
    }

    public function enterNode(Node $node) {
        if ($node instanceof Class_) {
            if ($this->cls !== null) {
                throw new Exception("{$this->fileName} has more than one class defined");
            }
            $this->cls = $node;
            return NodeTraverser::DONT_TRAVERSE_CHILDREN;
        }
        return parent::enterNode($node);
    }

    public function getCls(): Class_ {
        if ($this->cls === null) {
            throw new Exception("{$this->fileName} has no class defined");
        }
        return $this->cls;
    }
}
