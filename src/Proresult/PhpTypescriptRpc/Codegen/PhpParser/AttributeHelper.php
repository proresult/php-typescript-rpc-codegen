<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Codegen\PhpParser;

use Exception;
use phpDocumentor\Reflection\Fqsen;
use phpDocumentor\Reflection\Types\Null_;
use PhpParser\Node\Arg;
use PhpParser\Node\Attribute;
use PhpParser\Node\AttributeGroup;
use PhpParser\Node\Expr;
use PhpParser\Node\Expr\ClassConstFetch;
use PhpParser\Node\Expr\ConstFetch;
use PhpParser\Node\Identifier;
use PhpParser\Node\Name;
use PhpParser\Node\Scalar\LNumber;
use PhpParser\Node\Scalar\String_;
use PhpParser\Node\Stmt;
use PhpParser\Node\Stmt\Class_;
use PhpParser\Node\Stmt\ClassMethod;
use PhpParser\NodeTraverser;
use PhpParser\NodeVisitor\NameResolver;
use PhpParser\ParserFactory;
use Proresult\PhpTypescriptRpc\Codegen\Php\ClassConstantReference;
use Proresult\PhpTypescriptRpc\Codegen\Php\ClassReference;

/**
     * AttributeHelper gets the source for a file and extracts the parts we need through the use of php-parser lib instead of phpDocumentor lib.
     * Brought in for extracting Attributes, which the phpDocumentor lib doesn't support.
     */
class AttributeHelper {
    private string $fileSource;
    private string $fileName;
    /**
     * @var Stmt[]|null
     */
    private array|null $stmts = null;
    private Class_|null $cls = null;
    /**
     * @param string $fileName
     * @param string $fileSource
     */
    public function __construct(string $fileName, string $fileSource) {
        $this->fileName = $fileName;
        $this->fileSource = $fileSource;
    }

    public function parse(): void {
        if ($this->stmts === null) {
            $parserFactory = new ParserFactory();
            $parser = $parserFactory->create(ParserFactory::ONLY_PHP7);
            $this->stmts = $parser->parse($this->fileSource);
        }
    }

    public function getClass(): Class_ {
        $this->parse();
        if ($this->stmts === null) {
            throw new Exception("No stmts parsed (null) from file {$this->fileName}");
        } elseif (empty($this->stmts)) {
            throw new Exception("No stmts parsed (0) from file {$this->fileName}");
        } else {
            $traverser = new NodeTraverser();
            $nameResolver = new NameResolver();
            $traverser->addVisitor($nameResolver);
            $namespacedStmts = $traverser->traverse($this->stmts);
            $traverser->removeVisitor($nameResolver);
            $classExtractor = new SingleClassExtractingNodeVisitor($this->fileName);
            $traverser->addVisitor($classExtractor);
            $traverser->traverse($namespacedStmts);
            $this->cls = $classExtractor->getCls();
        }
        return $this->cls;
    }

    public function getMethod(string $name): ClassMethod {
        $cls = $this->getClass();
        $m = $cls->getMethod($name);
        if (empty($m)) {
            throw new Exception("Method \"{$name}\" not found in class \"{$cls->name}\" in file \"{$this->fileName}\"");
        }
        return $m;
    }

    /**
     * @param string $name
     *
     * @return Attribute[]
     * @throws Exception
     */
    public function methodAttributes(string $methodName): array {
        $ret = [];
        foreach ($this->getMethod($methodName)->getAttrGroups() as $attrGroup) {
            foreach ($attrGroup->attrs as $attr) {
                $ret[] = $attr;
            }
        }
        return $ret;
    }

    public function methodAttribute(string $methodName, string $attributeName): ?Attribute {
        $found = array_values(array_filter($this->methodAttributes($methodName), fn(Attribute $attribute) => $attribute->name->toString() === $attributeName));
        return match (count($found)) {
            0 => null,
            1 => $found[0],
            default => throw new Exception("More than one $attributeName attribute found on method $methodName"),
        };
    }

    public function methodHasAttribute(string $methodName, string $attributeName): bool {
        return $this->methodAttribute($methodName, $attributeName) !== null;
    }

    /** @return AttributeGroup[] PHP attribute groups */
    public function classAttributes(): array {
        return $this->getClass()->attrGroups;
    }

    public function classAttribute(string $attributeName): ?Attribute {
        $ret = null;
        foreach ($this->classAttributes() as $attributeGroup) {
            foreach ($attributeGroup->attrs as $attr) {
                if ($attr->name->toString() === $attributeName) {
                    if ($ret === null) {
                        $ret = $attr;
                    } else {
                        $cls = $this->getClass();
                        throw new Exception("More than one attribute with name \"{$attributeName}\" on class \"{$cls->name}\" in file \"{$this->fileName}\"");
                    }
                }
            }
        }
        return $ret;
    }

    public function attributeArg(?Attribute $attr, int $argNumber): ?Arg {
        if ($attr !== null) {
            $argCount = count($attr->args);
            if ($argCount >= $argNumber) {
                return $attr->args[$argNumber - 1];
            }
        }
        return null;
    }

    public function classConstantReferenceValue(Expr $value): ClassConstantReference {
        if ($value instanceof ClassConstFetch) {
            $cls = $value->class;
            if ($cls instanceof Name) {
                if ($cls->isFullyQualified()) {
                    $classReference = ClassReference::fromFqsen(new Fqsen($cls->toCodeString()));
                    if ($value->name instanceof Identifier) {
                        $name = $value->name->name;
                        return new ClassConstantReference($classReference, $name);
                    } else {
                        throw new Exception("Attribute name is not an instance of \"Identifier\"");
                    }
                } else {
                    throw new Exception("{$cls->toString()} is not a fully qualified name");
                }
            } else {
                throw new Exception("Attribute value class is not an instance of \"Name\"");
            }
        } else {
            throw new Exception("{$value->getType()} is not an instance of \"ClassConstFetch\"");
        }
    }

    public function classReferenceFromAttributeArg(?Arg $arg): ?ClassReference {
        if ($arg !== null) {
            $classConstantReference = $this->classConstantReferenceValue($arg->value);
            if ($classConstantReference->constantName === "class") {
                return $classConstantReference->classReference;
            } else {
                throw new Exception(
                    "attribute argument must be a class constant with a \"::class\" suffix, referring to an existing class type"
                );
            }
        } else {
            return null;
        }
    }

    private function resolveClassConstFetch(ClassConstFetch $const): mixed {
        $constantRef = $this->classConstantReferenceValue($const);
        return constant($constantRef->toLongCodeString()); // NB: Evaluates the php code!
    }

    public function stringFromAttributeArg(?Arg $arg): ?string {
        if ($arg !== null) {
            $value = $arg->value;
            if ($value instanceof ClassConstFetch) {
                /** @var mixed $constantVal */
                $constantVal = $this->resolveClassConstFetch($value);
                if (is_string($constantVal)) {
                    return $constantVal;
                } else {
                    throw new Exception("attribute argument refers to a constant that does not resolve to a string value");
                }
            } elseif ($value instanceof String_) {
                return $value->value;
            } else {
                throw new Exception("attribute argument is not a string constant, hardcoded string value or null constant");
            }
        } else {
            return null;
        }
    }

    public function boolFromAttributeArg(?Arg $arg): ?bool {
        if ($arg !== null) {
            $value = $arg->value;
            if ($value instanceof ClassConstFetch) {
                /** @var mixed $constantVal */
                $constantVal = $this->resolveClassConstFetch($value);
                if (is_bool($constantVal)) {
                    return $constantVal;
                } else {
                    throw new Exception("attribute argument refers to a constant that does not resolve to a boolean value");
                }
            } elseif ($value instanceof ConstFetch) {
                /** @var mixed $constantVal */
                $constantVal = constant($value->name->toCodeString()); // NB: Evaluates the php code!
                if (is_bool($constantVal)) {
                    return $constantVal;
                } else {
                    throw new Exception("attribute argument refers to a hardcoded constant, but it is not a boolean ({$constantVal})");
                }
            } else {
                throw new Exception("attribute argument is not a boolean constant or hardcoded boolean value");
            }
        } else {
            return null;
        }
    }

    public function intFromAttributeArg(?Arg $arg): ?int {
        if ($arg !== null) {
            $value = $arg->value;
            if ($value instanceof ClassConstFetch) {
                /** @var mixed $constantVal */
                $constantVal = $this->resolveClassConstFetch($value);
                if (is_int($constantVal)) {
                    return $constantVal;
                } else {
                    throw new Exception("attribute argument refers to a constant that does not resolve to a int value");
                }
            } elseif ($value instanceof LNumber) {
                return $value->value;
            } else {
                throw new Exception("attribute argument is not a int constant or hardcoded int value. (" . get_debug_type($value) . ")");
            }
        } else {
            return null;
        }
    }
}
