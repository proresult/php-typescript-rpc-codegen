<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Codegen;

use Exception;
use phpDocumentor\Reflection\File\LocalFile;
use phpDocumentor\Reflection\Php\File as PhpFile;
use phpDocumentor\Reflection\Php\Project;
use phpDocumentor\Reflection\Php\ProjectFactory;
use Proresult\PhpTypescriptRpc\Codegen\Php\NamespaceAdjuster;
use Proresult\PhpTypescriptRpc\Codegen\Php\NamespaceRep;
use Proresult\PhpTypescriptRpc\Codegen\Php\RpcClass;
use Proresult\PhpTypescriptRpc\Codegen\PhpParser\AttributeHelper;

/**
 * Class PhpRpcAdapterGenerator Parses the input php rpc class, and creates a adapter class in the output dir that
 * bridges between the defined rpc class and the rpc framework
 * request/response cycle.
 *
 * @package Proresult\PhpTypescriptRpc
 */
class PhpRpcAdapterGenerator {
    protected string $inputBaseDir;
    protected string $outputBaseDir;
    protected NamespaceRep $baseNamespace;
    protected NamespaceAdjuster $rpcAdapterNamespaceAdjuster;
    private ?Project $project = null;
    private string $phpHeaderContent;
    public function __construct(
        string $inputBaseDir,
        string $outputBaseDir,
        NamespaceRep $baseNamespace,
        NamespaceAdjuster $rpcAdapterNamespaceAdjuster,
        string $phpHeaderContent,
    ) {
        $this->inputBaseDir = $inputBaseDir;
        $this->outputBaseDir = $outputBaseDir;
        $this->baseNamespace = $baseNamespace;
        $this->rpcAdapterNamespaceAdjuster = $rpcAdapterNamespaceAdjuster;
        $this->phpHeaderContent = $phpHeaderContent;
    }

    protected function project(): Project {

        if ($this->project === null) {
            $files          = array_map(fn(\SplFileInfo $fileInfo) => new LocalFile($fileInfo->getPathname()), Files::phpFileArray($this->inputBaseDir));
            $projectFactory = ProjectFactory::createInstance();
            $project  = $projectFactory->create("PHP Input", $files);
            assert($project instanceof Project);
            $this->project = $project;
        }
        return $this->project;
    }

    /**
     * @return PhpFile[]
     */
    public function getInputFiles(): array {
        return $this->project()->getFiles();
    }

    public function rpcClassForFile(PhpFile $file): RpcClass {
        $classes = array_values($file->getClasses());
        if (count($classes) !== 1) {
            throw new Exception(
                "There must be only one class defined in each rpc class source file (got: " . count($classes) . " classes from file {$file->getPath()})"
            );
        }
        $class = $classes[0];
        $attributeHelper = new AttributeHelper($file->getName(), $file->getSource());
        return RpcClass::fromClass($class, $attributeHelper);
    }

    /**
     * Create the rpc adapter class filepath from given input file and the $inputBaseDir and $outputBaseDir values.
     * The generated rpc adapter class file should be based in a namespace
     * @param PhpFile $phpFile
     *
     * @return string
     */
    public function outputFilepath(string $inputFilepath, string $outputClassname): string {

        if (str_starts_with($inputFilepath, $this->inputBaseDir)) {
            $end = substr($inputFilepath, strlen($this->inputBaseDir));
            $subdir = dirname($end);
            return $this->outputBaseDir . $subdir . '/' . $outputClassname . ".php";
        } else {
            throw new Exception("$inputFilepath does not start with inputBaseDir ({$this->inputBaseDir})");
        }
    }

    public function processAllFiles(): void {
        foreach ($this->getInputFiles() as $phpFile) {
            $rpcClass = $this->rpcClassForFile($phpFile);
            $coder = new PhpRpcAdapterCoder($this->baseNamespace, $this->rpcAdapterNamespaceAdjuster, $rpcClass, $this->phpHeaderContent);
            $outputFilepath = $this->outputFilepath($phpFile->getPath(), $coder->adapterClassName());
            Files::writeFile($outputFilepath, $coder->phpCode());
        }
    }
}
