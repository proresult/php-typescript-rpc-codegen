<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Codegen;

use phpDocumentor\Reflection\Php\Method as PhpMethod;
use phpDocumentor\Reflection\Php\Visibility;

/**
     * Class Parsing Some shared util/helper code used across various parsing code in the project.
     *
     * @package Proresult\PhpTypescriptRpc
     */
class Parsing {
    /**
     * Filters out constructor, non-public, and static methods, since they don't define rpc endpoint methods.
     * Used both in the php rpc adapter generator and typescript rpc transpiler.
     * @param PhpMethod[] $methods all methods extracted from a "rpc class" definition.
     *
     * @return PhpMethod[] the methods that are to be considered as rpc methods from those passed in
     */
    public static function filterRpcMethods(array $methods): array {
        $filtered = array_filter($methods, fn(PhpMethod $method) =>
                $method->getName() != "__construct" &&
                !$method->isStatic() &&
                $method->getVisibility() == Visibility::PUBLIC_);
        return array_values($filtered);
    }
}
