<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen\Exceptions;

class ErrorHandlerException extends \Exception {
    public int $errno;
    public string $errstr;
    public ?string $errfile;
    public ?int $errline;

    /**
     * ErrorHandlerException constructor.
     *
     * @param int         $errno
     * @param string      $errstr
     * @param string|null $errfile
     * @param int|null    $errline
     */
    public function __construct(int $errno, string $errstr, ?string $errfile, ?int $errline) {
        $this->errno   = $errno;
        $this->errstr  = $errstr;
        $this->errfile = $errfile;
        $this->errline = $errline;

        parent::__construct($errstr);
    }

    public static function setErrorHandler(): void {
        $cb = function (int $errno, string $errstr, ?string $errfile, ?int $errline): void {
            throw new ErrorHandlerException($errno, $errstr, $errfile, $errline);
        };
            set_error_handler($cb, E_WARNING | E_ERROR);
    }
}
