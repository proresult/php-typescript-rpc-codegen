<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Codegen\Exceptions;

use phpDocumentor\Reflection\DocBlock;
use Throwable;

class InvalidDocblock extends \Exception {
    public function __construct(string $msg, DocBlock $block, ?Throwable $cause = null) {
        $location = $block->getLocation();
        $summary = mb_substr($block->getSummary(), 0, 50);
        $txt = $msg . " for docblock \"{$summary}\"... (at line {$location?->getLineNumber()}, column {$location?->getColumnNumber()}).";
        parent::__construct($txt, previous: $cause);
    }
}
