<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Codegen\Exceptions;

use phpDocumentor\Reflection\Type;

class SingleTypeExpected extends \Exception {
    public function __construct(Type $type) {

        $msg = "Expected a (nullable) single type, got {$type}";
        parent::__construct($msg);
    }
}
