<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen\Exceptions;

    use Throwable;

class FileProcessingException extends \Exception {
    public string $sourceFile;

    /**
     * FileException constructor.
     */
    public function __construct(string $sourceFile, ?string $msg, ?Throwable $cause = null) {
        $this->sourceFile = $sourceFile;
        $txt = $msg != null ? $msg : " Exception during processing. ";
        parent::__construct($this->sourceFile . ": $txt", previous: $cause);
    }
}
