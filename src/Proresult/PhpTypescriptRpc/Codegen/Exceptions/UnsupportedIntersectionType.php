<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Codegen\Exceptions;

use phpDocumentor\Reflection\Types\Intersection;

class UnsupportedIntersectionType extends \Exception {
    public function __construct(Intersection $intersection) {
        parent::__construct("Intersection type \"$intersection\" is not supported for use in PHP Typescript RPC (yet).");
    }
}
