<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen\Exceptions;

class InvalidName extends \Exception {
    private string $input;

    /**
     * InvalidName constructor.
     *
     * @param string $input
     */
    public function __construct(string $input, string $msg) {
        $this->input = $input;
        parent::__construct($msg);
    }

    public function getInput(): string {
        return $this->input;
    }
}
