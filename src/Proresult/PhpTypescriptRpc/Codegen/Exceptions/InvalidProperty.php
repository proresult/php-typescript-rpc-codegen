<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen\Exceptions;

    use phpDocumentor\Reflection\Php\Property as PhpProperty;
    use Throwable;

class InvalidProperty extends \Exception {
    /**
     * InvalidProperty constructor.
     */
    public function __construct(string $msg, PhpProperty $property, ?Throwable $cause = null) {
        $location = $property->getLocation();
        $txt = $msg . " for property \"{$property->getName()}\" (at line {$location->getLineNumber()}, column {$location->getColumnNumber()}).";
        parent::__construct($txt, previous: $cause);
    }
}
