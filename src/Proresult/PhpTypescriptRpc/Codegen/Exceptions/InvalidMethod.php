<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Codegen\Exceptions;

use phpDocumentor\Reflection\Php\Method as PhpMethod;
use Throwable;

class InvalidMethod extends \Exception {
    public function __construct(string $msg, PhpMethod $method, ?Throwable $cause = null) {
        $location = $method->getLocation();
        $txt = $msg . " for property \"{$method->getName()}\" (at line {$location->getLineNumber()}, column {$location->getColumnNumber()}).";
        parent::__construct($txt, previous: $cause);
    }
}
