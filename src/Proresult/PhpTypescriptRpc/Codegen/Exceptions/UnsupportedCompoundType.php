<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Codegen\Exceptions;

use phpDocumentor\Reflection\Types\Compound;

class UnsupportedCompoundType extends \Exception {
    public function __construct(Compound $compound) {
        parent::__construct(
            "Compound type \"$compound\" is not supported for use in PHP Typescript RPC. " .
            "Must be all native json types or an object and a null"
        );
    }
}
