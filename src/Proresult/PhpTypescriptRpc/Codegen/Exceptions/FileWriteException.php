<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen\Exceptions;

class FileWriteException extends \Exception {
    public string $filepath;

    /**
     * FileWriteException constructor.
     *
     * @param string $filepath
     */
    public function __construct(string $filepath, string $msg, ?\Throwable $cause = null) {
        $this->filepath = $filepath;
        parent::__construct("Could not write {$filepath}: " . $msg, 0, $cause);
    }
}
