<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen;

    use Proresult\PhpTypescriptRpc\Codegen\Typescript\ModulePathPrefixAdjustments;

class TypescriptModelTranspiler extends AbstractTypescriptTranspiler {
    /**
     * TypescriptModelTranspiler constructor.
     *
     * @param string                     $inputBaseDir
     * @param string                     $outputBaseDir
     * @param ModulePathPrefixAdjustments $modulePathPrefixAdjustment
     */
    public function __construct(
        string $inputBaseDir,
        string $outputBaseDir,
        ModulePathPrefixAdjustments $modulePathPrefixAdjustment,
        string $fileHeaderContent = ""
    ) {
        parent::__construct($inputBaseDir, $outputBaseDir, $modulePathPrefixAdjustment, $fileHeaderContent);
    }

    /**
     * Transpile all php files found in $inputBaseDir, and write resulting typescript files to $outputBaseDir
     */
    public function run(): void {
        $this->transpiled = $this->transpileAll();
        foreach ($this->transpiled as $output) {
            $output->writeTypescriptModelFile($output->typescriptFilePath($this->outputBaseDir));
        }
    }
}
