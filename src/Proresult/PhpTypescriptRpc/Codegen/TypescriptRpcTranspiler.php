<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Codegen;

use Proresult\PhpTypescriptRpc\Codegen\Php\NamespaceRep;
use Proresult\PhpTypescriptRpc\Codegen\Typescript\ModulePathPrefixAdjustments;

class TypescriptRpcTranspiler extends AbstractTypescriptTranspiler {
    /** @var NamespaceRep The root namespace of rpc classes being transpiled. Rpc method URL paths will be generated below this namespace. */
    protected NamespaceRep $baseRpcNamespace;
    /**
     * TypescriptRpcTranspiler constructor.
     *
     * @param NamespaceRep $baseRpcNamespace
     */
    public function __construct(
        string $inputBaseDir,
        string $outputBaseDir,
        ModulePathPrefixAdjustments $modulePathPrefixAdjustment,
        NamespaceRep $baseRpcNamespace,
        string $fileHeaderContent = ""
    ) {
        parent::__construct($inputBaseDir, $outputBaseDir, $modulePathPrefixAdjustment, $fileHeaderContent);
        $this->baseRpcNamespace = $baseRpcNamespace;
    }

    public function run(): void {
        $this->transpiled = $this->transpileAll();
        foreach ($this->transpiled as $output) {
            $output->writeTypescriptRpcFile($output->typescriptFilePath($this->outputBaseDir), $this->baseRpcNamespace);
        }
    }
}
