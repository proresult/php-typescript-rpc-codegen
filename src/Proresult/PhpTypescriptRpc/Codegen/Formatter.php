<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Codegen;

class Formatter {
    public static function withSemicolonEnd(string $tsLine): string {

        return !empty($tsLine) ? $tsLine . ";" : "";
    }

    /** @param Typescripter[] $typescripters */
    public static function arrayToTypescript(array $typescripters, int $indent = 0): string {
        $indentTxt = str_repeat("\t", $indent);
        return join(PHP_EOL . $indentTxt, array_map(fn(Typescripter $typescripter) => self::withSemicolonEnd($typescripter->typescript()), $typescripters));
    }

    /** Return string that indents given number of spaces */
    public static function indentSpaces(int $spaces): string {
        $ret = "";
        for ($i = 0; $i < $spaces; $i++) {
            $ret .= " ";
        }
        return $ret;
    }
}
