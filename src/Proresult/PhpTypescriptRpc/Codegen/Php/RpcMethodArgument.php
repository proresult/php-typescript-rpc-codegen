<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Codegen\Php;

use Exception;

/**
     * Container for declared rpc method argument names and types
     */
class RpcMethodArgument {
    public string $paramName;
    public ClassReference|ScalarReference $paramType;
    public string|null $defaultValue;
    public bool $nullable;

    /**
     * @param string                         $paramName
     * @param ClassReference|ScalarReference $paramType
     */
    public function __construct(string $paramName, ClassReference|ScalarReference $paramType, string|null $defaultValue, bool $nullable) {
        $this->paramName = $paramName;
        $this->paramType = $paramType;
        $this->defaultValue = $defaultValue;
        $this->nullable = $nullable;
    }

    public function isClassReference(): bool {
        return $this->paramType instanceof ClassReference;
    }

    public function isScalarReference(): bool {
        return $this->paramType instanceof ScalarReference;
    }

    public function isNullable(): bool {
        return $this->nullable;
    }

    public function hasDefaultValue(): bool {
        return $this->defaultValue !== null;
    }

    /**
     * @throws Exception
     */
    public function getClassReference(): ClassReference {

        if ($this->paramType instanceof ClassReference) {
            return $this->paramType;
        } else {
            throw new Exception("RpcMethodArgument {$this->paramName} is not a ClassReference");
        }
    }

    /**
     * @throws Exception
     */
    public function getScalarReference(): ScalarReference {

        if ($this->paramType instanceof ScalarReference) {
            return $this->paramType;
        } else {
            throw new Exception("RpcMethodArgument {$this->paramName} is not a ScalarReference");
        }
    }
}
