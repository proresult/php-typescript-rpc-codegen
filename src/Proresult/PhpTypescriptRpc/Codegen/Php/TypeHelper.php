<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Codegen\Php;

use phpDocumentor\Reflection\Type;
use phpDocumentor\Reflection\Types\Compound;
use phpDocumentor\Reflection\Types\Null_;
use phpDocumentor\Reflection\Types\Nullable;
use phpDocumentor\Reflection\Types\Object_;
use Proresult\PhpTypescriptRpc\Codegen\Exceptions\SingleTypeExpected;

class TypeHelper {
    private Null_ $nullTypeInstance;
    public function __construct(private Type $type) {
        $this->nullTypeInstance = new Null_();
    }

    public function isNull(): bool {
        return $this->type instanceof Null_;
    }

    public function isNullable(): bool {
        return $this->type instanceof Nullable;
    }

    public function isCompoundWithNull(): bool {
        if ($this->type instanceof Compound) {
            return $this->type->contains($this->nullTypeInstance);
        }
        return false;
    }

    public function isScalarReference(): bool {
        return ScalarReference::isAllowedType($this->type->__toString());
    }

    public function isObjectReference(): bool {
        return $this->type instanceof Object_;
    }

    /**
     * Remove any nullable or null from this type and return it without them
     */
    public function withoutNullable(): Type {
        if ($this->type instanceof Nullable) {
            $recurse = new TypeHelper($this->type->getActualType());
            return $recurse->withoutNullable();
        }
        if ($this->type instanceof Compound) {
            $typesWithoutNull = [];
            foreach ($this->type->getIterator() as $typePart) {
                if (!($typePart instanceof Null_)) {
                    $recurse = new TypeHelper($typePart);
                    $typesWithoutNull[] = $recurse->withoutNullable();
                }
            }
            if (count($typesWithoutNull) === 1) {
                // If there is only one type left from the compound after filtering for null, return the base type
                // instead of compound type.
                return $typesWithoutNull[0];
            }
            return new Compound($typesWithoutNull);
        }
        return $this->type;
    }

    /**
     * If the given type is a single type, a nullable, or a compound type consisting of just one type and a null,
     * return that type. Otherwise, throw exception
     * @return Type
     */
    public function singleTypeExceptNull(): Type {
        if ($this->isScalarReference() || $this->isObjectReference()) {
            return $this->type;
        }
        if ($this->type instanceof Nullable) {
            $notNull = new TypeHelper($this->type->getActualType());
            return $notNull->singleTypeExceptNull();
        } elseif ($this->type instanceof Compound) {
            // Loop through the "subtypes" in the compound and set singleType to the one actual type other than null.
            // Fail if it turns out there is more than one actual type in the compound.
            $singleType = null;
            foreach ($this->type->getIterator() as $typePart) {
                $typePartHelper = new TypeHelper($typePart);
                if ($typePartHelper->isScalarReference() || $typePartHelper->isObjectReference()) {
                    if ($singleType === null) {
                        $singleType = $typePartHelper->singleTypeExceptNull();
                    } else {
                        throw new SingleTypeExpected($this->type);
                    }
                }
            }
            if ($singleType === null) {
                throw new SingleTypeExpected($this->type);
            }
            return $singleType;
        } else {
            throw new SingleTypeExpected($this->type);
        }
    }
}
