<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Codegen\Php;

class RpcMethodArguments {
    /**
     * @var RpcMethodArgument[]
     */
    public array $arguments = [];
    /**
     * @param RpcMethodArgument[] $arguments
     */
    public function __construct(array $arguments = []) {
        $this->arguments = $arguments;
    }

    public function addArgument(RpcMethodArgument $argument): void {
        $this->arguments[] = $argument;
    }

    /**
     * @return string[]
     */
    public function getUses(): array {
        $ret = [];
        foreach ($this->arguments as $argument) {
            if ($argument->isClassReference()) {
                $use = $argument->paramType->__toString();
                if (!in_array($use, $ret)) {
                    $ret[] = $use;
                }
            }
        }
        return $ret;
    }

    public function count(): int {
        return count($this->arguments);
    }

    public function first(): RpcMethodArgument {
        return $this->arguments[0];
    }
}
