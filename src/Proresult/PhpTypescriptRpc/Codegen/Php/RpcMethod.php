<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Codegen\Php;

use Exception;
use phpDocumentor\Reflection\Php\Method as PhpMethod;
use phpDocumentor\Reflection\Types\Object_;
use PhpParser\Node\Arg;
use Proresult\PhpTypescriptRpc\Codegen\PhpParser\AttributeHelper;
use Proresult\PhpTypescriptRpc\Server\Attributes\AuthCheck;
use Proresult\PhpTypescriptRpc\Server\Attributes\Get;
use Proresult\PhpTypescriptRpc\Server\Attributes\Retryable;

class RpcMethod {
    public RpcMethodArguments $arguments;
    public ClassReference $responseType;
    public ClassReference $rpcClassRef;
    public string $methodName;
    private bool $isGetMethod;
    public ?AuthCheck $authCheck;
    public ?Retryable $retryable;
/**
     * RpcMethod constructor.
     *
     * @param RpcMethodArguments $arguments
     * @param ClassReference     $returnType
     * @param ClassReference     $rpcClassRef
     * @param string             $methodName
     * @param bool               $isGetMethod
     */
    public function __construct(
        RpcMethodArguments $arguments,
        ClassReference $returnType,
        ClassReference $rpcClassRef,
        string $methodName,
        bool $isGetMethod,
        ?AuthCheck $authCheck,
        ?Retryable $retryable,
    ) {
        $this->arguments    = $arguments;
        $this->responseType = $returnType;
        $this->rpcClassRef = $rpcClassRef;
        $this->methodName   = $methodName;
        $this->isGetMethod = $isGetMethod;
        $this->authCheck = $authCheck;
        $this->retryable = $retryable;
    }

    public function isGetMethod(): bool {
        return $this->isGetMethod;
    }
    public function isPostMethod(): bool {
        return !$this->isGetMethod();
    }

    /**
     * @throws Exception
     */
    private static function argumentsFromMethod(PhpMethod $method, bool $hasGetAttribute): RpcMethodArguments {
        $arguments     = $method->getArguments();
        $argumentCount = count($arguments);
        if ($argumentCount !== 1 && !$hasGetAttribute) {
            throw new Exception(
                "Rpc POST methods must have exactly one argument. Found $argumentCount arguments to method {$method->getFqsen()}"
            );
        }
        $ret = new RpcMethodArguments();
        foreach ($arguments as $argument) {
            if ($argument->isVariadic()) {
                throw new Exception(
                    "Rpc method argument cannot be variadic. ({$method->getFqsen()} {$method->getName()}, {$argument->getName()})"
                );
            }
            if ($argument->isByReference()) {
                throw new Exception(
                    "Rpc method argument cannot be by reference. ({$method->getFqsen()} {$method->getName()}, {$argument->getName()})"
                );
            }
            if ($argument->getType() === null) {
                throw new Exception(
                    "Rpc method argument must have a type declared. Found none for method {$method->getFqsen()} " .
                    "{$method->getName()}, argument {$argument->getName()}"
                );
            }
            $typeHelper = new TypeHelper($argument->getType());
            $isNullable = $typeHelper->isNullable() || $typeHelper->isCompoundWithNull();
            $argumentType = $typeHelper->singleTypeExceptNull();
            if ($hasGetAttribute) {
                if (ScalarReference::isAllowedType($argumentType->__toString())) {
                    $typ     = new ScalarReference($argumentType->__toString());
                    $default = $argument->getDefault();
                    $ret->addArgument(new RpcMethodArgument($argument->getName(), $typ, $default, $isNullable));
                } else {
                    throw new Exception(
                        "Rpc GET method argument must be a scalar type. Found {$argumentType} for method " .
                        "{$method->getFqsen()} {$method->getName()}, argument {$argument->getName()}"
                    );
                }
            } else {
                if ($argumentType instanceof Object_) {
                    if ($argumentType->getFqsen() !== null) {
                        $typ     = ClassReference::fromFqsen($argumentType->getFqsen());
                        $default = $argument->getDefault();
                        if ($default !== null) {
                            throw new Exception(
                                "Rpc POST method argument must not have a default value defined. Found \"$default\" " .
                                "for method {$method->getFqsen()} {$method->getName()}, argument {$argument->getName()}"
                            );
                        }
                        $ret->addArgument(new RpcMethodArgument($argument->getName(), $typ, $default, false));
                    } else {
                        throw new Exception(
                            "Rpc POST method argument type must have \"Fqsen\", got null. Method {$method->getFqsen()}, argument {$argument->getName()}."
                        );
                    }
                } elseif ($isNullable) {
                    throw new Exception(
                        "Rpc POST method argument type cannot be nullable. got " .
                        "{$argumentType->__toString()} for method {$method->getFqsen()} {$method->getName()}, " .
                        "argument {$argument->getName()}"
                    );
                } else {
                    throw new Exception(
                        "Rpc POST method argument type must be a class type. got {$argumentType} for method " .
                        "{$method->getFqsen()} {$method->getName()}, argument {$argument->getName()}"
                    );
                }
            }
        }

        return $ret;
    }

    /**
     * @param array<array-key, Arg> $args
     *
     * @return ?Arg
     */
    private static function findArgByName(array $args, string $name): ?Arg {
        foreach ($args as $arg) {
            if ($arg->name?->name === $name) {
                return $arg;
            }
        }
        return null;
    }

    private static function isPositionalArg(Arg $arg): bool {
        return $arg->name === null;
    }

    /**
     * If there is a #AuthCheck attribute on the method, this returns it's value
     * @param PhpMethod       $method
     * @param AttributeHelper $attributeHelper
     *
     * @return AuthCheck|null
     */
    private static function authCheckFromMethod(PhpMethod $method, AttributeHelper $attributeHelper): ?AuthCheck {
        $attr = $attributeHelper->methodAttribute($method->getName(), AuthCheck::class);
        if ($attr !== null) {
            $args = $attr->args;
            $argCount = count($args);
            // Arguments can be positional or named.
            // Positional argument(s) cannot come after named.
            // First try resolving arguments by position, then by name if they are not found as positional.
            // methodName is the first argument
            $methodArg = $argCount > 0 && self::isPositionalArg($args[0]) ? $args[0] : self::findArgByName($args, 'methodName');
            // includeParams is the second argument
            $includeParamsArg = $argCount > 1 && self::isPositionalArg($args[1]) ? $args[1] : self::findArgByName($args, 'includeParams');
            // paramMaps is the "rest" of the arguments, no matter if it is named or not.
            // If less than three argumens are provided we look for it as one named argument.
            $paramMapsArgs = $argCount > 2 ? array_slice($args, 2, null, true) : self::findArgByName($args, 'paramMaps');
            if ($paramMapsArgs instanceof Arg) {
                $paramMapsArgs = [$paramMapsArgs];
            }

            $methodName = $attributeHelper->stringFromAttributeArg($methodArg);
            $includeParams = $attributeHelper->boolFromAttributeArg($includeParamsArg);
            if ($paramMapsArgs !== null) {
                $paramMaps = array_map(fn(Arg $arg) => $attributeHelper->stringFromAttributeArg($arg), $paramMapsArgs);
                $paramMaps = array_filter($paramMaps, fn(?string $paramMap) => $paramMap !== null);
                foreach ($paramMaps as $paramMap) {
                    // Check that first character in any param map is not dollar sign $, the map should not have these
                    // since the code generator prefixes with them when generating the php code.
                    if (mb_substr($paramMap, 0, 1) === '$') {
                        throw new Exception(
                            "#AuthCheck parameter map value (\"{$paramMap}\") should not be prefixed with the \"$\" character. " .
                            "The dollar sign is added when generating the php code."
                        );
                    }
                }

                /* @var $paramMaps string[] */
                return new AuthCheck($methodName, $includeParams, ...$paramMaps);
            } else {
                return new AuthCheck($methodName, $includeParams);
            }
        } else {
            return null;
        }
    }

    private static function retryableFromMethod(PhpMethod $method, AttributeHelper $attributeHelper): ?Retryable {
        $attr = $attributeHelper->methodAttribute($method->getName(), Retryable::class);
        if ($attr !== null) {
            // Retryable attribute set on method, find the retryable count number from attribute args.
            // There is only one argument defined for Retryable attribute at this time, so don't need advanced resolve
            // like AuthCheck attribute has. Reconsider if we add more arguments to the attribute definition.
            $maxRetryCount = $attributeHelper->intFromAttributeArg($attr->args[0] ?? null);
            if ($maxRetryCount !== null) {
                return new Retryable($maxRetryCount);
            } else {
                return new Retryable(); // Use default maxRetryCount value defined in attribute type definition (2)
            }
        } else {
            return null;
        }
    }

    /**
     * @throws Exception
     */
    public static function fromMethod(PhpMethod $method, AttributeHelper $attributeHelper): self {
        $methodName = $method->getName();
        $hasGetAttribute = $attributeHelper->methodHasAttribute($methodName, Get::class);
        $arguments = self::argumentsFromMethod($method, $hasGetAttribute);
        $returnType = $method->getReturnType();
        if (!$returnType instanceof Object_) {
            throw new Exception("Rpc method must have return type of a class. got {$returnType} for method {$method->getFqsen()} {$method->getName()}");
        }
        if ($returnType->getFqsen() === null) {
            throw new Exception("Rpc method return type \"Fqsen\" was null for method {$method->getFqsen()} {$method->getName()}");
        }
        $responseTypeRef = ClassReference::fromFqsen($returnType->getFqsen());
        $rpcClassRef = ClassReference::fromFqsen($method->getFqsen());
        // If there is AuthCheck attribute, parse it
        $authCheck = self::authCheckFromMethod($method, $attributeHelper);
        // If there is Retryable attribute, parse it
        $retryable = self::retryableFromMethod($method, $attributeHelper);
        if ($retryable === null && $hasGetAttribute) {
            // For Get methods, we add a default Retryable if one is not explicitly set.
            $retryable = new Retryable();
        }
        return new RpcMethod($arguments, $responseTypeRef, $rpcClassRef, $methodName, $hasGetAttribute, $authCheck, $retryable);
    }

    /**
     * Create the rpc path to use for this method.
     * The rpc path follows this algorithm: /<SubNamespace>/<ClassName>/<methodName>
     * Where SubNamespace consists of all elements in the Rpc class namespace below given $baseNamespace. (May be none.)
     *
     * This way we can have a hierarchy of namespaced rpc classes.
     * Because of this it is also important to follow PSR-4 php naming standard, where all namespaces and classes are in init-caps, while method
     * names in rpc classes have camelCase.
     *
     * @param NamespaceRep $baseNamespace
     *
     * @return string
     * @throws Exception
     */
    public function rpcPath(NamespaceRep $baseNamespace): string {
        $classNs = NamespaceRep::fromString($this->rpcClassRef->namespace);
        $subNs = $classNs->below($baseNamespace);
        $namespacePath = !empty($subNs->components) ? "/" . join("/", $subNs->components) : "";
        return "{$namespacePath}/{$this->rpcClassRef->name}/{$this->methodName}";
    }

    public function requestBodyArgument(): RpcMethodArgument {

        if ($this->isPostMethod()) {
            if ($this->arguments->count() === 1) {
                return $this->arguments->first();
            } else {
                throw new Exception("Rpc method {$this->methodName} is POST, has {$this->arguments->count()} arguments defined. Should be 1.");
            }
        } else {
            throw new Exception("Rpc method {$this->methodName} is not a POST method");
        }
    }
}
