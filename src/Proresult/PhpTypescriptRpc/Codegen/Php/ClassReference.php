<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen\Php;

    use Exception;
    use phpDocumentor\Reflection\Fqsen;

class ClassReference {
    public string $namespace;
    public string $name;

    /**
     * ClassReference constructor.
     *
     * @param string $namespace
     * @param string $name
     */
    public function __construct(string $namespace, string $name) {
        $this->namespace = $namespace;
        $this->name      = $name;
    }

    public static function fromFqsen(Fqsen $fqsen): self {
        $parts = array_filter(explode('\\', $fqsen->__toString()), fn(string $part) => !empty($part));
        $lastPart = array_pop($parts);
        $namespace = join('\\', $parts);
        // If lastPart is not the same as $fqsen->getName() output, the given $fqsen is a reference to a member of the class.
        // Will then extract the classname from the last part instead of using $fqsen->getName()
        $name = $fqsen->getName();
        if ($lastPart !== $name) {
            $nameParts = explode("::", $lastPart);
            if (count($nameParts) !== 2) {
                throw new Exception("Expected Fqsen $fqsen name part to have two parts separated by colon, but got " . count($nameParts));
            }
            $name = $nameParts[0];
        }
        return new ClassReference($namespace, $name);
    }

    public function __toString(): string {
        return $this->namespace . "\\" . $this->name;
    }

    public function getFqsen(): Fqsen {
        return new Fqsen("\\" . $this->namespace . '\\' . $this->name);
    }
}
