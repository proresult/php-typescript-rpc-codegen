<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Codegen\Php;

use Exception;

class NamespaceRep {
    /**
     * @var string[]
     */
    public array $components = [];
/**
     * NamespaceRep constructor.
     *
     * @param string[] $components
     */
    public function __construct(array $components) {
        $this->components = array_values($components);
    }

    public function __toString(): string {
        $prefix = !empty($this->components) ? "\\" : "";
        return $prefix . join("\\", $this->components);
    }

    public static function fromString(string $ns): NamespaceRep {
        $components = array_filter(explode("\\", $ns), fn(string $component) => !empty($component));
        return new NamespaceRep($components);
    }

    /**
     * Resolve a new NamespaceRep consisting of the components of this that is "below" given other base namespace.
     * Used to create sub-paths when classes are in nested namespaces.
     * @param NamespaceRep $baseNamespace
     *
     * @return NamespaceRep
     */
    public function below(NamespaceRep $baseNamespace): NamespaceRep {
        $sub = $this->components;
        if (empty($sub)) {
            throw new Exception("Cannot resolve namespace below empty NamespaceRep");
        }
        foreach ($baseNamespace->components as $baseComponent) {
            if ($baseComponent === $sub[0]) {
                array_shift($sub);
            // Remove the equal component from $sub
            } else {
            // We return the resolved sub namespace as soon as we find a component that don't match.
                return new NamespaceRep($sub);
            }
        }
        return new NamespaceRep($sub); // When $baseNamespace is empty, this is essentially a noop copy of $this.
    }
}
