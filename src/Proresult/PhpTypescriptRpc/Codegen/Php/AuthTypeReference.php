<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Codegen\Php;

/**
     * Holds the info from parsing the #AuthType attribute on a rpc class.
     */
class AuthTypeReference {
    public function __construct(public ClassReference $controllerClass, public ?string $defaultMethodName,) {
    }
}
