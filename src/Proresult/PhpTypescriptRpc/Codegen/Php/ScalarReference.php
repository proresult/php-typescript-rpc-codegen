<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Codegen\Php;

class ScalarReference {
    public const ALLOWED_TYPE_NAMES = ["string", "int", "float", "bool"];
    public string $typeName;

    public static function isAllowedType(string $typeName): bool {
        return in_array($typeName, self::ALLOWED_TYPE_NAMES);
    }

    /**
     * @param string $typeName
     */
    public function __construct(string $typeName) {
        if (self::isAllowedType($typeName)) {
            $this->typeName = $typeName;
        } else {
            throw new \Exception("\"{$typeName}\" is not a allowed ScalarReference typeName value");
        }
    }

    public function __toString(): string {
        return $this->typeName;
    }
}
