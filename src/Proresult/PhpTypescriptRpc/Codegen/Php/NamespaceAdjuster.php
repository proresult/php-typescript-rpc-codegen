<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Codegen\Php;

use Exception;

/**
     * Class NamespaceAdjuster Used to adjust the namespace of the rpc adapter class based on the rpc class being processed.
     *
     * It will remove the namespace string given in $remove constructor argument and add $add namespace string in place of it.
     *
     * @package Proresult\PhpTypescriptRpc\Php
     */
class NamespaceAdjuster {
    public string $remove;
    public string $add;
/**
     * NamespaceAdjuster constructor.
     */
    public function __construct(string $remove, string $add) {
        $this->remove = $remove;
        $this->add    = $add;
    }


    public function adjust(string $namespace): string {

        if (str_contains($namespace, $this->remove)) {
            return str_replace($this->remove, $this->add, $namespace);
        } else {
            throw new Exception("namespace \"$namespace\" does not contain namespace part to remove ({$this->remove})");
        }
    }
}
