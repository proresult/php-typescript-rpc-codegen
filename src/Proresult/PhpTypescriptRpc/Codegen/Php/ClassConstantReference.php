<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Codegen\Php;

class ClassConstantReference {
    public function __construct(public ClassReference $classReference, public string $constantName,) {
    }

    public function toShortCodeString(): string {

        return "{$this->classReference->name}::{$this->constantName}";
    }

    public function toLongCodeString(): string {

        return "{$this->classReference->getFqsen()}::{$this->constantName}";
    }
}
