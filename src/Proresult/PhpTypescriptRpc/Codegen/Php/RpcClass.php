<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Codegen\Php;

use Exception;
use phpDocumentor\Reflection\Php\Class_;
use phpDocumentor\Reflection\Php\Method as PhpMethod;
use Proresult\PhpTypescriptRpc\Codegen\Parsing;
use Proresult\PhpTypescriptRpc\Codegen\PhpParser\AttributeHelper;
use Proresult\PhpTypescriptRpc\Server\Attributes\AuthType;
use Proresult\PhpTypescriptRpc\Server\Attributes\RequestType;

/**
     * Holds the data required to output php code for a rpc adapter class.
     *
     * A rpc adapter class is the bridge between the request/response and a defined rpc class method in the rpc framework server.
     *
     * @package Proresult\PhpTypescriptRpc\Php
     */
class RpcClass {
    public const DEFAULT_INIT_FROM_REQUEST_ARGUMENT_TYPE = '\\Psr\\Http\\Message\\ServerRequestInterface';
    public ClassReference $rpcClassRef;
    public RpcMethods $rpcMethods;
    public ?ClassReference $requestClassRef;
    public ?AuthTypeReference $authTypeRef;
/**
     * RpcAdapter constructor.
     *
     * @param ClassReference $rpcClassRef
     * @param ClassReference $requestClassRef
     * @param ClassReference $responseClassRef
     * @param string         $rpcMethodName
     */
    public function __construct(ClassReference $rpcClassRef, RpcMethods $rpcMethods, ?ClassReference $requestClassRef, ?AuthTypeReference $authTypeRef) {
        $this->rpcClassRef      = $rpcClassRef;
        $this->rpcMethods = $rpcMethods;
        $this->requestClassRef = $requestClassRef;
        $this->authTypeRef = $authTypeRef;
    }


    public function className(): string {
        return $this->rpcClassRef->name;
    }

    private static function rpcClassRef(Class_ $rpcClass): ClassReference {
        return ClassReference::fromFqsen($rpcClass->getFqsen());
    }

    public static function validate(Class_ $class, ?ClassReference $requestClassRef): void {
        // Check that there is a static initFromRequest function
        $initFromRequest = array_values(
            array_filter(
                $class->getMethods(),
                fn(PhpMethod $method) => $method->getName() == "initFromRequest" && $method->isStatic()
            )
        );
        if (count($initFromRequest) === 1) {
            $initFromRequest = $initFromRequest[0];
        // Check that it has correct argument
            $requestArguments = $initFromRequest->getArguments();
            if (count($requestArguments) !== 1) {
                throw new Exception("rpc class {$class->getFqsen()} initFromRequest must have exactly one argument");
            }
            $requestArgument = $requestArguments[0];
            $requestArgumentType = $requestArgument->getType();
            if ($requestArgumentType === null) {
                throw new Exception("rpc class {$class->getFqsen()} initFromRequest argument must have type declaration");
            }
            // Check that initFromRequest argument type is correct
            $requiredRequestArgumentType = $requestClassRef !== null ?
                $requestClassRef->getFqsen()->__toString() : self::DEFAULT_INIT_FROM_REQUEST_ARGUMENT_TYPE;

            if ($requestArgumentType->__toString() !== $requiredRequestArgumentType) {
                throw new Exception(
                    "rpc class {$class->getFqsen()} initFromRequest argument must have type " . $requiredRequestArgumentType .
                    " (got: {$requestArgumentType})"
                );
            }
            // Check that it has correct return type
            $returnType = $initFromRequest->getReturnType();
            if ($returnType->__toString() !== $class->getFqsen()->__toString()) {
                throw new Exception(
                    "rpc class {$class->getFqsen()} initFromRequest must return instance of itself. " .
                    "Got: {$returnType}, Expected {$class->getFqsen()}"
                );
            }
        } else {
            throw new Exception("rpc class {$class->getFqsen()} does not implement the required static initFromRequest function");
        }
        // Check that there is not a method named handleRequest, this is reserved for the generated adapter class.
        $handleRequestExists = !empty(array_values(array_filter($class->getMethods(), fn(PhpMethod $method) => $method->getName() == "handleRequest")));
        if ($handleRequestExists) {
            throw new Exception("rpc class {$class->getFqsen()} has method handleRequest. This is a reserved method name for the generated rpc adapter class.");
        }
    }

    private static function classReferenceFromAttributeArg(Class_ $class, AttributeHelper $attributeHelper, string $attributeName): ?ClassReference {
        try {
            return $attributeHelper->classReferenceFromAttributeArg(
                $attributeHelper->attributeArg($attributeHelper->classAttribute($attributeName), 1)
            );
        } catch (Exception $e) {
            throw new Exception("Could not get class reference from rpc class {$class->getFqsen()}, attribute {$attributeName}", previous: $e);
        }
    }


    private static function authTypeReferenceFromAttribute(Class_ $class, AttributeHelper $attributeHelper): ?AuthTypeReference {
        $controllerClass = self::classReferenceFromAttributeArg($class, $attributeHelper, AuthType::class);
        try {
            $defaultMethodName = $attributeHelper->stringFromAttributeArg($attributeHelper->attributeArg($attributeHelper->classAttribute(AuthType::class), 2));
        } catch (Exception $e) {
            throw new Exception("Could not get auth type default method name attribute value for rpc class {$class->getFqsen()}", previous: $e);
        }
        if ($controllerClass !== null) {
            return new AuthTypeReference($controllerClass, $defaultMethodName);
        } else {
            return null;
        }
    }


    public static function fromClass(Class_ $class, AttributeHelper $attributeHelper): self {
        $requestClassRef = self::classReferenceFromAttributeArg($class, $attributeHelper, RequestType::class);
        $authClassRef = self::authTypeReferenceFromAttribute($class, $attributeHelper);
        self::validate($class, $requestClassRef);
        $rpcClassRef = self::rpcClassRef($class);
        $rawMethods = Parsing::filterRpcMethods($class->getMethods());
        $rpcMethods = new RpcMethods(array_map(fn(PhpMethod $phpMethod) => RpcMethod::fromMethod($phpMethod, $attributeHelper), $rawMethods));
        return new RpcClass($rpcClassRef, $rpcMethods, $requestClassRef, $authClassRef);
    }
}
