<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Codegen\Php;

use phpDocumentor\Reflection\Php\Class_;
use phpDocumentor\Reflection\Php\Method as PhpMethod;

class RpcMethods {
    /**
     * @var RpcMethod[]
     */
    public array $methods = [];
    /**
     * RpcMethods constructor.
     *
     * @param RpcMethod[] $methods
     */
    public function __construct(array $methods) {
        $this->methods = $methods;
    }


    public function addMethod(RpcMethod $method): void {
        $this->methods[] = $method;
    }

    /**
     * @return string[] Returns array of all unique class references for request and response arguments for the $methods in this class
     */
    public function getUses(): array {
        $ret = [];
        foreach ($this->methods as $method) {
            foreach ($method->arguments->getUses() as $use) {
                if (!in_array($use, $ret)) {
                    $ret[] = $use;
                }
            }
            $responseRef = $method->responseType->__toString();
            if (!in_array($responseRef, $ret)) {
                $ret[] = $responseRef;
            }
        }
        return $ret;
    }
}
