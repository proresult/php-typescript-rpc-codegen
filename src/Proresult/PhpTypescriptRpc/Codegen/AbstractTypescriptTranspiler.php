<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Codegen;

use phpDocumentor\Reflection\File\LocalFile;
use phpDocumentor\Reflection\Php\File as PhpFile;
use phpDocumentor\Reflection\Php\Project;
use phpDocumentor\Reflection\Php\ProjectFactory;
use Proresult\PhpTypescriptRpc\Codegen\Typescript\File;
use Proresult\PhpTypescriptRpc\Codegen\Typescript\ModulePathPrefixAdjustments;

abstract class AbstractTypescriptTranspiler {
    protected string $inputBaseDir;
    protected string $outputBaseDir;
    protected Project $project;
    protected ModulePathPrefixAdjustments $modulePathPrefixAdjustment;
    protected string $fileHeaderContent = "";
    /** @var File[]|null */
    protected ?array $transpiled = null;
    /**
     * TypescriptModelTranspiler constructor.
     *
     * @param string                     $inputBaseDir
     * @param string                     $outputBaseDir
     * @param ModulePathPrefixAdjustments $modulePathPrefixAdjustment
     */
    public function __construct(
        string $inputBaseDir,
        string $outputBaseDir,
        ModulePathPrefixAdjustments $modulePathPrefixAdjustment,
        string $fileHeaderContent
    ) {
        $this->inputBaseDir               = $inputBaseDir;
        $this->outputBaseDir              = $outputBaseDir;
        $this->modulePathPrefixAdjustment = $modulePathPrefixAdjustment;
        $files = array_map(fn(\SplFileInfo $fileInfo) => new LocalFile($fileInfo->getPathname()), Files::phpFileArray($this->inputBaseDir));
        $projectFactory = ProjectFactory::createInstance();
        $project  = $projectFactory->create("PHP Input", $files);
        assert($project instanceof Project);
        $this->project = $project;
        $this->fileHeaderContent = $fileHeaderContent;
    }

    /**
     * @return PhpFile[]
     */
    public function getInputFiles(): array {
        return $this->project->getFiles();
    }


    protected function transpileFile(PhpFile $inputFile): File {
        $typescriptFile = new File($inputFile, $this->modulePathPrefixAdjustment, $this->fileHeaderContent);
        // But return the base File instance
        return $typescriptFile;
    }

    /**
     * @return File[]
     */
    protected function transpileAll(): array {
        return array_map(fn(PhpFile $inputFile) => $this->transpileFile($inputFile), $this->getInputFiles());
    }

    /**
     * Transpile all php files found in $inputBaseDir, and write resulting typescript files to $outputBaseDir
     * @return void
     */
    abstract public function run(): void;
    /**
     * @return File[]|null
     */
    public function getTranspiledFiles(): ?array {
        return $this->transpiled;
    }
}
