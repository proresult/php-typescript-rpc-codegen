<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Codegen\Typescript;

class ModulePathPrefixAdjustments {
    /**
     * @var ModulePathPrefixAdjustment[]
     */
    public array $adjustments = [];
    /**
     * ModulePathPrefixAdjustements constructor.
     */
    public function __construct(ModulePathPrefixAdjustment ...$adjustments) {
        $this->adjustments = $adjustments;
    }


    public static function none(): ModulePathPrefixAdjustments {
        return new ModulePathPrefixAdjustments(ModulePathPrefixAdjustment::none());
    }

    public function adjust(ModulePath $modulePath): ModulePath {
        $adjusted = $modulePath;
        foreach ($this->adjustments as $adjustment) {
            $adjusted = $adjustment->adjust($adjusted);
        }
        return $adjusted;
    }
}
