<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen\Typescript;

    use Proresult\PhpTypescriptRpc\Codegen\Exceptions\InvalidName;

    /**
     * Class Name Used to hold a single identifier name. Cannot contain whitespace, dots, slashes or other illegal characters
     *
     * @package Proresult\PHPTypescriptRPC\Typescript
     */
class Name {
    private string $name;

    public const ILLEGAL_REGEX = "/[^[:alnum:]\-]/";

    /**
     * Name constructor.
     *
     * @param string $name
     */
    public function __construct(string $name) {
        $name = trim($name);
        if (empty($name)) {
            throw  new InvalidName($name, "Empty name is not allowed");
        }
        if (!self::legalCheck($name)) {
            throw new InvalidName($name, "Input \"$name\" failed the legalcheck regex");
        }
        $this->name = $name;
    }

    public static function legalCheck(string $name): bool {
        if (str_starts_with($name, "@")) {
            $name = substr($name, 1);
        }
        return preg_match(self::ILLEGAL_REGEX, $name) === 0;
    }

    /**
     * @return string
     */
    public function getName(): string {
        return $this->name;
    }

    public function __toString(): string {
        return $this->name;
    }

    /**
     * @param string ...$names
     *
     * @return Name[]
     */
    public static function arrayFromStrings(string ...$names): array {
        $ret = [];
        foreach ($names as $name) {
            if (!empty(trim($name))) {
                $ret[] = new Name($name);
            }
        }
        return $ret;
    }
}
