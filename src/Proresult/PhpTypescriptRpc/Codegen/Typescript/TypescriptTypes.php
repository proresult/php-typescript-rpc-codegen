<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen\Typescript;

    use Proresult\PhpTypescriptRpc\Codegen\Typescripter;

    // TODO Rewrite this so that it don't just stuff various types into the $types string. Keep a proper AST
    // representation until typescript code generation is run.
class TypescriptTypes implements Typescripter {
    private string $types;

    private ImportSet $importSet;

    /**
     * Types constructor.
     *
     * @param string $types The typescript definition string
     */
    public function __construct(string $types, ImportSet $importSet = null) {
        $this->types = $types;
        $this->importSet = $importSet !== null ? $importSet : ImportSet::empty();
    }

    public function typescript(): string {
        return $this->getTypes();
    }

    public function getTypes(): string {
        return $this->types;
    }

    /**
     */
    public function getImportSet(): ImportSet {
        return $this->importSet;
    }

    /**
     * Add null type to this types if it is not already added.
     *
     * XXX: This is, as this whole class, an abomination. Should be solved in another way, but it seems to work as is.
     */
    public function addNull(): void {
        $typs = explode("|", $this->types);
        $typs = array_map(fn(string $typ) => trim($typ), $typs);
        if (!in_array("null", $typs)) {
            $this->types .= " | null";
        }
    }
}
