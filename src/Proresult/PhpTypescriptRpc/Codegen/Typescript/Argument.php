<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Codegen\Typescript;

use phpDocumentor\Reflection\Php\Method as PhpMethod;
use phpDocumentor\Reflection\Php\Argument as PhpArgument;
use phpDocumentor\Reflection\DocBlock as PhpDocBlock;
use phpDocumentor\Reflection\DocBlock\Tags\Param;
use Proresult\PhpTypescriptRpc\Codegen\Exceptions\InvalidConversion;
use Proresult\PhpTypescriptRpc\Codegen\Exceptions\InvalidDocblock;
use Proresult\PhpTypescriptRpc\Codegen\Exceptions\InvalidMethod;
use Proresult\PhpTypescriptRpc\Codegen\Typescripter;

class Argument implements Typescripter {
    protected PhpArgument $phpArgument;
    protected ?PhpDocBlock $phpDocBlock;
    protected PhpMethod $phpMethod;
    protected bool $hasDefault;
    /**
     * Argument constructor.
     *
     * @param PhpArgument $phpArgument
     */
    public function __construct(PhpMethod $phpMethod, PhpArgument $phpArgument) {
        $this->phpMethod = $phpMethod;
        $this->phpDocBlock = $phpMethod->getDocBlock();
        $this->phpArgument = $phpArgument;
        $this->hasDefault = $phpArgument->getDefault() !== null;
    }

    /**
     * @return PhpArgument
     */
    public function getPhpArgument(): PhpArgument {

        return $this->phpArgument;
    }

    protected ?TypescriptTypes $types = null;

    /**
     * @param string $paramName
     *
     * @return Param|null
     * @throws InvalidDocblock
     */
    protected function getDocBlockParamByName(string $paramName): ?Param {
        $ret = null;
        $docBlock = $this->phpDocBlock;
        if ($docBlock !== null) {
            $paramTags = $docBlock->getTagsWithTypeByName("param");
            foreach ($paramTags as $paramTag) {
                if ($paramTag instanceof Param) {
                    if ($paramTag->getVariableName() === $paramName) {
                        if ($ret === null) {
                                $ret = $paramTag;
                        } else {
                            throw new InvalidDocblock("More than one param tag with name $paramName found", $docBlock);
                        }
                    }
                }
            }
        }
        return $ret;
    }

    public function getTypescriptTypes(): TypescriptTypes {
        try {
            if ($this->types === null) {
                $arg = $this->getPhpArgument();
                $t = $arg->getType();
                if ($t === null || TypeConversions::isArrayWithoutType($t)) {
        // Try getting type from docblock
                    $docblockParam = $this->getDocBlockParamByName($arg->getName());
                    if ($docblockParam !== null) {
                            $t = $docblockParam->getType();
                    }
                }
                if ($t === null) {
                    throw new InvalidMethod(
                        "No type found for argument {$arg->getName()} to method {$this->phpMethod->getName()}",
                        $this->phpMethod
                    );
                }
                if (TypeConversions::isArrayWithoutType($t)) {
                    throw new InvalidMethod(
                        "Array defined without value type for argument {$arg->getName()} to method {$this->phpMethod->getName()}",
                        $this->phpMethod
                    );
                }
                $this->types = TypeConversions::typeToTypescript($t);
                if ($arg->getDefault() !== null) {
                    // Arg has default value on backend, so we allow the typecript argument to be null (it it isn't already).
                    // A null argument will not be sent, so the backend will insert its default value instead.
                    $this->types->addNull();
                }
            }
            return $this->types;
        } catch (InvalidConversion $ivc) {
            throw new InvalidMethod("Invalid typescript conversion", $this->phpMethod, $ivc);
        }
    }

    public function name(): string {
        return $this->getPhpArgument()->getName();
    }

    public function typescript(): string {

        return "{$this->name()}: {$this->getTypescriptTypes()->typescript()}";
    }
}
