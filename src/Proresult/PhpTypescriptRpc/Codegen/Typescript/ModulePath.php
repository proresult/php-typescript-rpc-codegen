<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen\Typescript;

    use phpDocumentor\Reflection\Fqsen;
    use Proresult\PhpTypescriptRpc\Codegen\Typescripter;

    /** Typescript representation of the php namespace of a file */
class ModulePath implements Typescripter {
    /**
     * @var Name[]
     */
    public array $elements;

    public bool $isGenerated = true;

    /**
     * To avoid having to resolve relative file paths we just use a alias and absolute paths throughout the generated code.
     * This must be setup as a paths alias in tsconfig.js (https://christianlydemann.com/simpler-typescript-paths-with-path-aliases/)
     */
    public const ABSOLUTE_PREFIX = "@/generated/";

    /**
     * ModulePath constructor.
     *
     * @param Name[] $elements
     */
    public function __construct(array $elements, bool $isGenerated = true) {
        $this->elements = $elements;
        $this->isGenerated = $isGenerated;
    }

    public function typescript(): string {
        $prefix = $this->isGenerated ? self::ABSOLUTE_PREFIX : "";
        return "'" . $prefix . $this->__toString() . "'";
    }

    public function __toString(): string {
        $elems = array_map(fn(Name $n) => $n->getName(), $this->elements);
        return join("/", $elems);
    }

    public static function fromFqsen(Fqsen $fqsen): ModulePath {
        $parts = explode("\\", $fqsen->__toString());
        $partNames = Name::arrayFromStrings(...$parts);
        return new ModulePath($partNames);
    }

    public function withoutPrefix(ModulePath $prefix): ModulePath {
        if ($this->hasPrefix($prefix)) {
            $elems = $this->elements;
            $elems = array_slice($elems, count($prefix->elements));
            return new ModulePath($elems, $this->isGenerated);
        } else {
            return $this;
        }
    }

    /**
     * @param ModulePath $prefix
     *
     * @return bool True if this has all the same elements as $prefix as the first elements
     */
    public function hasPrefix(ModulePath $prefix): bool {
        if (count($this->elements) < count($prefix->elements)) {
            return false;
        }
        for ($i = 0; $i < count($prefix->elements); $i++) {
            $prefixElement = $prefix->elements[$i];
            $thisElement = $this->elements[$i];
            if ($prefixElement->getName() != $thisElement->getName()) {
                return false;
            }
        }
        return true;
    }

    public function withPrefix(ModulePath $prefix): ModulePath {
        $elems = array_merge($prefix->elements, $this->elements);
        return new ModulePath($elems, $prefix->isGenerated);
    }

    public function appendName(Name $name): ModulePath {
        $elems = $this->elements;
        $elems[] = $name;
        return new ModulePath($elems);
    }

    public function withoutLastName(): ModulePath {
        $elems = $this->elements;
        if (!empty($elems)) {
            array_pop($elems);
        }
        return new ModulePath($elems);
    }

    /**
     * Create and return the filepath this module should be written to when creating typescript output.
     * @param string $rootPath This filepath will be prepended
     *
     * @return string
     */
    public function typescriptFilepath(string $rootPath): string {
        return rtrim($rootPath, "/") . "/" . $this->__toString() . ".ts";
    }
}
