<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen\Typescript;

    use Exception;
    use phpDocumentor\Reflection\Type;
    use phpDocumentor\Reflection\Types\Array_;
    use phpDocumentor\Reflection\Types\Boolean;
    use phpDocumentor\Reflection\Types\Compound;
    use phpDocumentor\Reflection\Types\Float_;
    use phpDocumentor\Reflection\Types\Integer;
    use phpDocumentor\Reflection\Types\Intersection;
    use phpDocumentor\Reflection\Types\Mixed_;
    use phpDocumentor\Reflection\Types\Null_;
    use phpDocumentor\Reflection\Types\Nullable;
    use phpDocumentor\Reflection\Types\Object_;
    use phpDocumentor\Reflection\Types\String_;
    use Proresult\PhpTypescriptRpc\Codegen\Exceptions\InvalidConversion;
    use Proresult\PhpTypescriptRpc\Codegen\Exceptions\UnsupportedCompoundType;
    use Proresult\PhpTypescriptRpc\Codegen\Exceptions\UnsupportedIntersectionType;

class TypeConversions {
    public static function typeToTypescript(Type $type): TypescriptTypes {
        return match (true) {
            $type instanceof Array_ => self::arrayToTypescript($type),
            $type instanceof Boolean => new TypescriptTypes("boolean"),
            $type instanceof Compound => self::compoundToTypescript($type),
            $type instanceof Float_ => new TypescriptTypes("number"),
            $type instanceof Integer => new TypescriptTypes("number"),
            $type instanceof Intersection => self::intersectionToTypescript($type),
            $type instanceof Null_ => new TypescriptTypes("null"),
            $type instanceof Nullable => self::nullableToTypescript($type),
            $type instanceof Object_ => self::objectToTypescript($type),
            $type instanceof String_ => new TypescriptTypes("string"),

            default => throw new InvalidConversion("php type \"{$type->__toString()}\" cannot be converted to typescript")
        };
    }

    public static function arrayToTypescript(Array_ $arr): TypescriptTypes {
        $vType = self::typeToTypescript($arr->getValueType());
        return match (true) {
            $arr->getKeyType() instanceof Integer => new TypescriptTypes(
                $vType->getTypes() . "[]",
                $vType->getImportSet()
            ), // Simple index based array
            $arr->getKeyType() instanceof String_ => new TypescriptTypes(
                "{[key: string]: {$vType->getTypes()}}",
                $vType->getImportSet()
            ), // Associative array, convert to typescript dict
            default => throw new InvalidConversion(
                "array key type must be integer or string. Got: {$arr->getKeyType()}"
            ),
        };
    }


    /**
     * @param TypescriptTypes[] $types
     *
     * @return ImportSet
     */
    public static function importSetFromTypes(array $types): ImportSet {
        return array_reduce($types, fn(ImportSet $importSet, TypescriptTypes $type) => $importSet->concat($type->getImportSet()), ImportSet::empty());
    }

    public static function isNativeJson(Type $type): bool {
        return match (true) {
            $type instanceof Boolean => true,
            $type instanceof Float_ => true,
            $type instanceof Integer => true,
            $type instanceof Null_ => true,
            $type instanceof String_ => true,
            default => false,
        };
    }

    public static function compoundToTypescript(Compound $compound): TypescriptTypes {
        // Perform check:
        // We allow compound types that are all made of native json types (string | int | bool | null)...
        // We allow compound types that have exactly one object type and one null type.
        // All others are unsupported at current release.
        $hasNull = false;
        $oneObject = false;
        $allNative = true;
        foreach ($compound->getIterator() as $type) {
            $isNativeJson = self::isNativeJson($type);
            $isObject = $type instanceof Object_;
            $isNull = $type instanceof Null_;
            if (($isNull && $isObject) || ($isNativeJson && $isObject)) {
                throw new Exception("Logical breach! $type resolved to multiple disjoint kinds");
            }
            if ($isNull) {
                $hasNull = true;
            }
            if ($isObject) {
                if (!$oneObject) {
                    $oneObject = true;
                } else {
                    throw new UnsupportedCompoundType($compound);
                }
            }
            if (!$isNativeJson) {
                $allNative = false;
            }
        }
        $supported = $allNative || ($oneObject && $hasNull);
        if (!$supported) {
            throw new UnsupportedCompoundType($compound);
        }

        /**
         * @var TypescriptTypes[]
         */
        $types = [];
        foreach ($compound->getIterator() as $type) {
            $types[] = self::typeToTypescript($type);
        }
        $typesTypescripted = array_map(fn(TypescriptTypes $types) => $types->typescript(), $types);
        return new TypescriptTypes("(" . join(" | ", $typesTypescripted) . ")", self::importSetFromTypes($types));
    }

    public static function intersectionToTypescript(Intersection $intersection): TypescriptTypes {
        throw new UnsupportedIntersectionType($intersection);
        /* XXX Consider implementing this later. (Would need a solution for deserialization)
        $types = [];
        foreach ($intersection->getIterator() as $type) {
            $types[] = self::typeToTypescript($type);
        }

        return new TypescriptTypes("(".join(" & ", array_map(fn(TypescriptTypes $t) => $t->getTypes(), $types)).")", self::importSetFromTypes($types));
        */
    }

    public static function nullableToTypescript(Nullable $nullable): TypescriptTypes {
        $actual = self::typeToTypescript($nullable->getActualType());
        return new TypescriptTypes($actual->getTypes() . " | null", $actual->getImportSet());
    }

    public static function objectToTypescript(Object_ $object): TypescriptTypes {
        $fqsen = $object->getFqsen();
        if ($fqsen !== null) {
            $parts = explode("\\", $fqsen->__toString());
            $partNames = Name::arrayFromStrings(...$parts);
            $modulePath = new ModulePath($partNames);
            $classImport = new Import($modulePath, new Name($fqsen->getName()));
            $deserializeImport = new Import($classImport->path, new Name("deserialize{$classImport->name}"));
            return new TypescriptTypes($fqsen->getName(), ImportSet::init($classImport, $deserializeImport));
        } else {
            throw new InvalidConversion("Untyped \"object\" not allowed as property type");
        }
    }

    /**
     * @param Type $type
     *
     * @return bool True if given type is array without any specification of it's value type. (Native php type without docblock)
     */
    public static function isArrayWithoutType(Type $type): bool {
        if ($type instanceof Array_) {
            return $type->getValueType() instanceof Mixed_;
        }
        return false;
    }
}
