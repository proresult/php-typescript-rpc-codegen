<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen\Typescript;

    use Proresult\PhpTypescriptRpc\Codegen\Typescripter;

    /** Represents an import of something to typescript file */
class Import implements Typescripter {
    public ModulePath $path;

    public Name $name;

    /**
     * Import constructor.
     *
     * @param ModulePath $path
     * @param Name       $name
     */
    public function __construct(ModulePath $path, Name $name) {
        $this->path = $path;
        $this->name = $name;
    }


    public function typescript(): string {
        return "import { {$this->name} } from {$this->path->typescript()}";
    }

    public function withModulePathPrefixAdjusted(ModulePathPrefixAdjustments $adjustment): self {
        return new Import($adjustment->adjust($this->path), $this->name);
    }
}
