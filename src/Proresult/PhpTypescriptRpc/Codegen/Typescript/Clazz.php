<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen\Typescript;

    use phpDocumentor\Reflection\Php\Class_;
    use phpDocumentor\Reflection\Php\Method as PhpMethod;
    use phpDocumentor\Reflection\Php\Property as PhpProperty;
    use phpDocumentor\Reflection\Types\Array_;
    use phpDocumentor\Reflection\Types\Object_;
    use PhpParser\ConstExprEvaluationException;
    use PhpParser\ConstExprEvaluator;
    use PhpParser\Node\Expr;
    use PhpParser\Node\Expr\ClassConstFetch;
    use PhpParser\Node\Identifier;
    use PhpParser\Node\Name;
    use Proresult\PhpTypescriptRpc\Codegen\Formatter;
    use Proresult\PhpTypescriptRpc\Codegen\Parsing;
    use Proresult\PhpTypescriptRpc\Codegen\Php\TypeHelper;
    use Proresult\PhpTypescriptRpc\Codegen\PhpParser\AttributeHelper;

class Clazz {
    public const DESERIALIZER_INPUT_ARG_NAME = "d";

    private Class_ $class_;
    private AttributeHelper $attributeHelper;

    private ModulePathPrefixAdjustments $modulePathPrefixAdjustement;

    /**
     * Clazz constructor.
     *
     * @param Class_ $class_
     */
    public function __construct(Class_ $class_, ModulePathPrefixAdjustments $modulePathPrefixAdjustement, AttributeHelper $attributeHelper) {
        $this->class_                = $class_;
        $this->modulePathPrefixAdjustement = $modulePathPrefixAdjustement;
        $this->attributeHelper = $attributeHelper;
    }

    // phpcs:ignore PSR1.Methods.CamelCapsMethodName.NotCamelCaps -- The underscore triggers phpcs, but we want it there for consistency.
    public function getClass_(): Class_ {
        return $this->class_;
    }

    /**
     * @return Property[]
     */
    public function getProperties(): array {
        return array_values(array_map(fn(PhpProperty $p) => new Property($p), $this->getClass_()->getProperties()));
    }

    private function ensureEvaluatedType(mixed $evaluatedValue): array|float|int|string|null {
        if (is_array($evaluatedValue) || is_float($evaluatedValue) || is_int($evaluatedValue) || is_string($evaluatedValue) || is_null($evaluatedValue)) {
            return $evaluatedValue;
        }
        throw new \TypeError("evaluated constant value must be array|float|int|string|null. Got " . get_debug_type($evaluatedValue));
    }

    // This is a separate function because if not psalm complains about unused variables
    private function constEval(Name $cls, Identifier $name): mixed {
        $className = implode("\\", $cls->parts);
        $constName = $name->name;
        $evalExpr  = "return {$className}::{$constName};";
        return eval($evalExpr);
    }

    /**
     * @return Constant[]
     */
    public function getConstants(): array {
        // A bit weird to use "attributeHelper" to get the parsed constants. Should rename it/rewrite it to be something
        // more general purpose, since it holds the php parser functionality.
        $classConsts = $this->attributeHelper->getClass()->getConstants();
        $evaluator = new ConstExprEvaluator(function (Expr $expr): mixed {
            // For consts where value reference a const in another class we evaluate the fully qualified reference to
            // resolve the value. eval is dangerous, but here I believe we can be fairly certain that it's use is safe,
            // since the input is parsed php known to be a class constant reference.
            if ($expr instanceof ClassConstFetch) {
                if ($expr->class instanceof Name) {
                    if ($expr->name instanceof Identifier) {
                        return $this->constEval($expr->class, $expr->name);
                    } else {
                        throw new ConstExprEvaluationException("Const name parse error for {$expr->getType()}");
                    }
                }
            }
            throw new ConstExprEvaluationException(
                "Const expression of type {$expr->getType()} cannot be evaluated"
            );
        });
        $constants = []; // The return value
        foreach ($classConsts as $classConst) {
            if ($classConst->isPublic()) { // We only transpile consts declared public
                foreach ($classConst->consts as $const) {
                    $name           = $const->name->name;
                    $evaluatedValue = $this->ensureEvaluatedType($evaluator->evaluateDirectly($const->value));
                    $constants[]    = new Constant($name, $evaluatedValue);
                }
            }
        }
        return $constants;
    }

    /**
     * @return Method[]
     */
    public function getMethods(): array {
        $rawMethods = Parsing::filterRpcMethods($this->getClass_()->getMethods());
        return array_map(fn(PhpMethod $m) => new Method($m, $this->attributeHelper), $rawMethods);
    }

    private static function resolveToOnePropertyObjectType(Property $property): ?Object_ {
        $type = $property->reflectionType();
        // If the property is an array with a set value type from it's docblock, resolve to that.
        if ($type instanceof Array_) {
            $type = $type->getValueType();
        }
        // If the property type is nullable or compound with nullable, extract the not nullable type from it.
        $typeHelper = new TypeHelper($type);
        $type = $typeHelper->withoutNullable();
        // We've come up with a object type, return it.
        if ($type instanceof Object_) {
            return $type;
        }
        return null;
    }

    /**
     * Returns all unique imports used by properties declared in this class.
     */
    public function getPropertyImportSet(): ImportSet {
        $imports = new ImportSet();
        foreach ($this->getProperties() as $property) {
            $objectPropertyType = self::resolveToOnePropertyObjectType($property);
            if ($objectPropertyType !== null) {
                $propertyTypeFqsen = $objectPropertyType->getFqsen()?->__toString();
                if ($propertyTypeFqsen == $this->class_->getFqsen()) {
                    // Property is a object of same type as this class, a circular dependency.
                    // Should then not be added to import set, so continue without.
                    // NB: If we later add support for union (compound) object types, this will be a potential bug,
                    // since this just looks at the whole property type.
                    continue;
                }
            }
            $imports = $imports->concat($property->getImportSet());
        }
        return $imports;
    }

    public function getMethodsImportSet(): ImportSet {
        $importSet = new ImportSet();
        foreach ($this->getMethods() as $method) {
            $importSet = $importSet->concat($method->getImportSet());
        }
        return $importSet;
    }


    protected function typescriptName(): string {
        return $this->getClass_()->getName();
    }

    protected function typescriptProperties(): string {
        return Formatter::arrayToTypescript($this->getProperties(), 1);
    }

    protected function typescriptConstants(): string {
        return Formatter::arrayToTypescript($this->getConstants(), 0);
    }

    /**
     * For each model interface there will be exported a function named deserialize<ModelName> which will be used to map
     * from the parsed json serialization of the interface back to a interface instance. This is so that custom mapping
     * of special types can be performed.
     *
     * @return string
     */
    private function typescriptModelInterfaceDeserializer(): string {
        $d = self::DESERIALIZER_INPUT_ARG_NAME;
        $propDeserializations = join(PHP_EOL, array_map(fn(Property $property) => $property->getTypescriptDeserializer($d), $this->getProperties()));
        return <<<TS
export const deserialize{$this->typescriptName()} = ($d: any): {$this->typescriptName()} => ({
$propDeserializations
});
TS;
    }

    public function typescriptModelInterface(): string {
        return Formatter::arrayToTypescript(
            $this->getPropertyImportSet()
                 ->withModulePathPrefixAdjusted($this->modulePathPrefixAdjustement)
                 ->getImports()
        ) . PHP_EOL .
               <<<TS
export interface {$this->typescriptName()} {
    {$this->typescriptProperties()}
}
{$this->typescriptModelInterfaceDeserializer()}
{$this->typescriptConstants()}
TS;
    }

    public function typescriptModulePath(): ModulePath {
        $mp = ModulePath::fromFqsen($this->getClass_()->getFqsen());

        return $this->modulePathPrefixAdjustement->adjust($mp);
    }
}
