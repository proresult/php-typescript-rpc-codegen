<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Codegen\Typescript;

use Exception;
use Proresult\PhpTypescriptRpc\Codegen\Typescripter;

class Constant implements Typescripter {
    public function __construct(private string $name, private array|int|float|string|null $value) {
    }

    public function typescriptName(): string {

        return $this->name;
    }

    private function typescriptScalar(int|float|string|null $v): string {

        if (is_string($v)) {
            return "\"{$v}\"";
        }
        return $v !== null ? "{$v}" : "null";
    }

    private function typescriptArray(array $arr): string {

        $scalarVals = [];
        /** @var mixed $v */
        foreach ($arr as $v) {
            if (is_int($v) || is_float($v) || is_string($v) || is_null($v)) {
                $scalarVals[] = $this->typescriptScalar($v);
            } else {
                throw new Exception("const \"{$this->name}\" has array with element of invalid type (" . get_debug_type($v) . ")");
            }
        }
        return '[' . implode(', ', $scalarVals) . ']';
    }

    public function typescriptValue(): string {

        if (is_array($this->value)) {
            return $this->typescriptArray($this->value);
        } else {
            return $this->typescriptScalar($this->value);
        }
    }

    public function typescript(): string {

        return "export const {$this->typescriptName()} = {$this->typescriptValue()}";
    }
}
