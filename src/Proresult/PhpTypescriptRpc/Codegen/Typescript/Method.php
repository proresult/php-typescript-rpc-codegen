<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Codegen\Typescript;

use phpDocumentor\Reflection\DocBlock\Tags\Return_;
use phpDocumentor\Reflection\Php\Method as PhpMethod;
use phpDocumentor\Reflection\Php\Argument as PhpArgument;
use Proresult\PhpTypescriptRpc\Codegen\Exceptions\InvalidDocblock;
use Proresult\PhpTypescriptRpc\Codegen\Exceptions\InvalidMethod;
use Proresult\PhpTypescriptRpc\Codegen\Php\NamespaceRep;
use Proresult\PhpTypescriptRpc\Codegen\Php\RpcMethod;
use Proresult\PhpTypescriptRpc\Codegen\Php\RpcMethodArgument;
use Proresult\PhpTypescriptRpc\Codegen\Php\RpcMethodArguments;
use Proresult\PhpTypescriptRpc\Codegen\PhpParser\AttributeHelper;

class Method {
    private PhpMethod $method;
    private AttributeHelper $attributeHelper;
    public function __construct(PhpMethod $method, AttributeHelper $attributeHelper) {
        $this->method = $method;
        $this->attributeHelper = $attributeHelper;
    }

    public function getMethod(): PhpMethod {
        return $this->method;
    }

    public function name(): string {
        return $this->method->getName();
    }

    /** @var Argument[]|null */
    protected ?array $args = null;
    /**
     * @return Argument[]
     */
    public function getTypescriptArguments(): array {
        if ($this->args === null) {
            $this->args = array_map(fn(PhpArgument $phpArgument) => new Argument($this->getMethod(), $phpArgument), $this->getMethod()->getArguments());
        }
        return $this->args;
    }

    private function getDocBlockReturn(): ?Return_ {
        $ret = null;
        $dockBlock = $this->getMethod()->getDocBlock();
        if ($dockBlock !== null) {
            $returnTags = $dockBlock->getTagsWithTypeByName("return");
            foreach ($returnTags as $returnTag) {
                if ($returnTag instanceof Return_) {
                    if ($ret === null) {
                        $ret = $returnTag;
                    } else {
                        throw new InvalidDocblock("More than one return tag found", $dockBlock);
                    }
                }
            }
        }
        return $ret;
    }

    /**
     * @return TypescriptTypes The typescript return type of the method
     * @throws InvalidDocblock
     * @throws InvalidMethod
     * @throws \Proresult\PhpTypescriptRpc\Codegen\Exceptions\InvalidConversion
     */
    public function getTypescriptReturnTypes(): TypescriptTypes {
        $t = $this->getMethod()->getReturnType();
        if (TypeConversions::isArrayWithoutType($t)) {
        // Try getting type from docblock
            $docblockReturn = $this->getDocBlockReturn();
            if ($docblockReturn !== null) {
                $t = $docblockReturn->getType();
            }
        }
        if ($t === null) {
            throw new InvalidMethod("No return type found for method {$this->name()}", $this->getMethod());
        }
        if (TypeConversions::isArrayWithoutType($t)) {
            throw new InvalidMethod("Return type of array without value type defined for method {$this->name()}", $this->getMethod());
        }
        return TypeConversions::typeToTypescript($t);
    }

    public function getImportSet(): ImportSet {
        $importSet = new ImportSet();
        foreach ($this->getTypescriptArguments() as $argument) {
            $importSet = $importSet->concat($argument->getTypescriptTypes()->getImportSet());
        }
        $returnType = $this->getTypescriptReturnTypes();
        return $importSet->concat($returnType->getImportSet());
    }

    /**
     * @param Argument[] $args
     *
     * @return string The typescript code for defining the arguments in a comma-separated list
     */
    public function typescriptArgumentList(array $args): string {
        return implode(", ", array_map(fn(Argument $argument) => $argument->typescript(), $args));
    }

    /**
     * @return string The typescript code for adding the argument values into the params map.
     */
    public function typescriptArgumentParamsSetters(RpcMethodArguments $argContainer): string {
        return implode("\n\t", array_map(fn(RpcMethodArgument $argument) =>
            ($argument->isNullable() || $argument->hasDefaultValue() ? "if({$argument->paramName} !== null) " : "") .
                "params.set(\"{$argument->paramName}\", {$argument->paramName});", $argContainer->arguments));
    }

    public function typescriptFetchErrorHandling(RpcMethod $rpcMethod): string {
        // If retryable attribute is set, it decides what fetchErrorHandling should be
        if ($rpcMethod->retryable !== null) {
            return "{maxRetryCount: {$rpcMethod->retryable->maxRetryCount}}";
        } else {
            return "null";
        }
    }

    /**
     * @param NamespaceRep $baseRpcNamespace The base php namespace for where the Rpc classes begin processed to create
     * typescript client for is. The generated URL for the rpc method will be below this base.
     *
     * @return string
     * @throws InvalidDocblock
     * @throws InvalidMethod
     */
    public function typescriptFetchFunction(NamespaceRep $baseRpcNamespace): string {
        $rpcMethod = RpcMethod::fromMethod($this->getMethod(), $this->attributeHelper);
        $args = $this->getTypescriptArguments();
        $requestBody = "null";
        if ($rpcMethod->isPostMethod()) {
            if (count($args) === 1) {
                $requestBody = $args[0]->name();
            } else {
                throw new InvalidMethod("POST Rpc method {$this->name()} must have exactly one argument (the request object)", $this->getMethod());
            }
        }
        $result = $this->getTypescriptReturnTypes()->typescript();
        $rpcPath = $rpcMethod->rpcPath($baseRpcNamespace);
        $resultDeserializer = "deserialize{$result}";
        $requestMethod = $rpcMethod->isGetMethod() ? "GET" : "POST";
        $paramSetters = "";
        if ($rpcMethod->isGetMethod()) {
            $paramSetters = $this->typescriptArgumentParamsSetters($rpcMethod->arguments);
        }
        $requestOptions = "null"; // Never set from codegen
        $fetchErrorHandling = $this->typescriptFetchErrorHandling($rpcMethod);
        return
        <<<TS
export const {$this->name()} = ({$this->typescriptArgumentList($args)}): RpcFetchAdapter<{$result}> => {
    const params = new Map<string, ParamValue>();
    $paramSetters
    return new RpcFetchAdapter<{$result}>("$requestMethod", "$rpcPath", params, $requestBody, $resultDeserializer, $requestOptions, $fetchErrorHandling);
}
TS;
    }
}
