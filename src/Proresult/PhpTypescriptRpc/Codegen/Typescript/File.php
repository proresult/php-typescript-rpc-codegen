<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen\Typescript;

    use Exception;
    use phpDocumentor\Reflection\Php\File as PhpFile;
    use Proresult\PhpTypescriptRpc\Codegen\Exceptions\FileProcessingException;
    use Proresult\PhpTypescriptRpc\Codegen\Exceptions\InvalidDocblock;
    use Proresult\PhpTypescriptRpc\Codegen\Exceptions\InvalidMethod;
    use Proresult\PhpTypescriptRpc\Codegen\Files;
    use Proresult\PhpTypescriptRpc\Codegen\Formatter;
    use Proresult\PhpTypescriptRpc\Codegen\Php\NamespaceRep;
    use Proresult\PhpTypescriptRpc\Codegen\PhpParser\AttributeHelper;

class File {
    private PhpFile $phpFile;
    private ModulePathPrefixAdjustments $modulePathPrefixAdjustement;
    private AttributeHelper $attributeHelper;
    private string $headerContent;

    public const RPC_METHOD_FETCH_ADAPTER_IMPORT = "import { RpcFetchAdapter, ParamValue } from '@proresult/php-typescript-rpc';";

    /**
     * File constructor.
     *
     * @param PhpFile      $phpFile
     */
    public function __construct(PhpFile $phpFile, ModulePathPrefixAdjustments $modulePathPrefixAdjustement, string $headerContent) {
        $this->phpFile               = $phpFile;
        $this->modulePathPrefixAdjustement = $modulePathPrefixAdjustement;
        $this->attributeHelper = new AttributeHelper($phpFile->getName(), $phpFile->getSource());
        $this->headerContent = $headerContent;
    }

    public function getPhpFile(): PhpFile {
        return $this->phpFile;
    }

    public function getFilePath(): string {
        return $this->phpFile->getPath();
    }

    public function getAttributeHelper(): AttributeHelper {
        return $this->attributeHelper;
    }

    public function clazz(): Clazz {
        $underlying = $this->getPhpFile()->getClasses();
        if (empty($underlying)) {
            throw new FileProcessingException($this->getFilePath(), "No class definition found in file");
        }
        if (count($underlying) > 1) {
            throw new FileProcessingException($this->getFilePath(), "More than one class definition in a file is not allowed");
        }
        $cls = reset($underlying); // Get the first (and only) element from $underlying array.
        return new Clazz($cls, $this->modulePathPrefixAdjustement, $this->attributeHelper);
    }

    /**
     * @throws FileProcessingException
     */
    public function typescriptModelInterface(): string {
        try {
            return PHP_EOL . $this->clazz()->typescriptModelInterface() . PHP_EOL;
        } catch (\Exception $e) {
            throw new FileProcessingException($this->getFilePath(), msg: null, cause: $e);
        }
    }

    public function typescriptModulePath(): ModulePath {
        return $this->clazz()->typescriptModulePath();
    }

    public function typescriptFilePath(string $rootPath): string {
        return $this->typescriptModulePath()->typescriptFilepath($rootPath);
    }

    public function outputModulePathAdjusted(ImportSet $importSet): ImportSet {
        return $importSet->withModulePathPrefixAdjusted($this->modulePathPrefixAdjustement);
    }

    /**
     * @throws InvalidDocblock
     * @throws FileProcessingException
     * @throws InvalidMethod
     */
    public function typescriptRpc(NamespaceRep $baseRpcNamespace): string {
        $methods = $this->clazz()->getMethods();
        if (!empty($methods)) {
            // check that methods is below baseRpcNamespace
            foreach ($methods as $method) {
                if (!str_starts_with($method->getMethod()->getFqsen()->__toString(), $baseRpcNamespace->__toString())) {
                    throw new FileProcessingException(
                        $this->getFilePath(),
                        "Rpc method {$method->getMethod()->getFqsen()} not below base namespace {$baseRpcNamespace}"
                    );
                }
            }
            $methodImports = $this->outputModulePathAdjusted($this->clazz()->getMethodsImportSet());
            $methodsTs     = array_map(fn(Method $method) => $method->typescriptFetchFunction($baseRpcNamespace), $methods);

            return Formatter::arrayToTypescript($methodImports->getImports()) . PHP_EOL .
                   self::RPC_METHOD_FETCH_ADAPTER_IMPORT . PHP_EOL . PHP_EOL .
                   join(PHP_EOL, $methodsTs) . PHP_EOL;
        } else {
            throw new FileProcessingException($this->getFilePath(), "No rpc methods found");
        }
    }

    /**
     * @throws FileProcessingException
     */
    protected function writeFile(string $filePath, string $fileContent): void {
        try {
            Files::writeFile($filePath, $fileContent);
        } catch (Exception $e) {
            throw new FileProcessingException($this->getFilePath(), msg: null, cause: $e);
        }
    }

    /**
     * Write the transpiled typescript model interface to a path and filename generated from the resolved typescript module path, underneath given $rootPath.
     * All imports must also be written under the same $rootPath for things to work.
     *
     * @param string $rootPath The root path to write the typescript under. Should be mapped to the "@generated" prefix in project tsconfig.json
     *
     * @throws FileProcessingException
     */
    public function writeTypescriptModelFile(string $filePath): void {
        $typescript = $this->typescriptModelInterface();
        $this->writeFile($filePath, $this->headerContent . PHP_EOL . $typescript);
    }

    /**
     * @throws FileProcessingException
     * @throws InvalidMethod
     * @throws InvalidDocblock
     */
    public function writeTypescriptRpcFile(string $filePath, NamespaceRep $baseRpcNamespace): void {
        $typescript = $this->typescriptRpc($baseRpcNamespace);
        $this->writeFile($filePath, $this->headerContent . PHP_EOL . $typescript);
    }
}
