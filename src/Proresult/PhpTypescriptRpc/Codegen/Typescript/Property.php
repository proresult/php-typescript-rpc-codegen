<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen\Typescript;

    use Exception;
    use phpDocumentor\Reflection\Type;
    use phpDocumentor\Reflection\Types\Compound;
    use phpDocumentor\Reflection\Types\Integer;
    use phpDocumentor\Reflection\Types\Null_;
    use phpDocumentor\Reflection\Types\Nullable;
    use phpDocumentor\Reflection\Types\Object_;
    use phpDocumentor\Reflection\Types\String_;
    use Proresult\PhpTypescriptRpc\Codegen\Exceptions\InvalidConversion;
    use Proresult\PhpTypescriptRpc\Codegen\Exceptions\InvalidProperty;
    use Proresult\PhpTypescriptRpc\Codegen\Typescripter;
    use phpDocumentor\Reflection\Php\Property as PhpProperty;
    use phpDocumentor\Reflection\Types\Array_;

class Property implements Typescripter {
    private PhpProperty $property;

    /**
     * Property constructor.
     *
     * @param PhpProperty $property
     */
    public function __construct(PhpProperty $property) {
        $this->property = $property;
    }

    public function getProperty(): PhpProperty {
        return $this->property;
    }


    public function name(): string {
        return $this->getProperty()->getName();
    }

    protected ?TypescriptTypes $types = null;

    /**
     * @return Type
     * @throws InvalidProperty
     */
    public function reflectionType(): Type {
        $t = $this->getProperty()->getType();
        // XXX Should perhaps clean this up a bit and get better error messages if the problem is array without type declared.
        if ($t === null || TypeConversions::isArrayWithoutType($t)) {
            // Try getting type from docblock
            $docBlock = $this->getProperty()->getDocBlock();
            if ($docBlock !== null) {
                $tags = $docBlock->getTagsWithTypeByName("var");
                if (count($tags) === 1) {
                    $t = $tags[0]->getType();
                    if ($t === null) {
                        throw new InvalidProperty("No accepted declared type in code, and no type found in docblock declaration", $this->getProperty());
                    }
                } elseif (empty($tags)) {
                    throw new InvalidProperty("Required property var annotation found in docblock", $this->getProperty());
                } elseif (count($tags) > 1) {
                    throw new InvalidProperty("Too many docblock types defined", $this->getProperty());
                }
            }
        }
        if ($t === null) {
            throw new InvalidProperty("No type found in php code or associated docblock", $this->getProperty());
        }
        return $t;
    }

    public function getTypescriptTypes(): TypescriptTypes {
        try {
            if ($this->types === null) {
                $this->types = TypeConversions::typeToTypescript($this->reflectionType());
            }

            return $this->types;
        } catch (InvalidConversion $ivc) {
            throw new InvalidProperty("Invalid typescript conversion", $this->getProperty(), $ivc);
        }
    }

    public function getTypescriptDeserializer(string $objectName): string {
        $key = $this->name();
        $valueRef = "{$objectName}.{$key}";
        $noOpDeserialization = fn(): string => "$key: $valueRef,";
        $objectDeserializationCall = fn(TypescriptTypes $vType, string $vRef): string => "deserialize{$vType->getTypes()}({$vRef})";
        $objectDeserialization = fn(TypescriptTypes $vType): string => "$key: {$objectDeserializationCall($vType, $valueRef)},";
        $nullableDeserializationWrapper = fn(string $deserializeCall): string => "{$valueRef} !== null && {$valueRef} !== undefined ? $deserializeCall : null";
        $nullableObjectDeserialization = fn(Object_ $object): string =>
            "$key: {$nullableDeserializationWrapper($objectDeserializationCall(TypeConversions::objectToTypescript($object), $valueRef))},";

        $t = $this->reflectionType();
        if (TypeConversions::isNativeJson($t)) {
            return $noOpDeserialization();
        } elseif ($t instanceof Compound) {
            // To keep things simple, compound can only be union of json native types, including null, or a object type and a null
            // Loop through all types and check
            // XXX Consider extending this to support more compound types later
            /** @var Object_ | null $hasObjectType If compound type has a object type this is set to it. */
            $hasObjectType = null;
            $hasNull = false;
            foreach ($t->getIterator() as $partType) {
                if (!TypeConversions::isNativeJson($partType)) {
                    if ($partType instanceof Object_) {
                        if ($hasObjectType === null) {
                            $hasObjectType = $partType;
                            continue;
                        } else {
                            throw new Exception("Compound type $t has more than one Object_ type. This is not supported");
                        }
                    }
                    throw new Exception("Cannot make deserialization for Compound type $t. $partType is not json native, and not otherwise supported");
                }
                if ($partType instanceof Null_) {
                    $hasNull = true;
                }
            }

            if ($hasObjectType !== null) {
                if (!$hasNull) {
                    throw new Exception("Compound type $t has Object_ type, but not Null_.");
                }
                // Create deserialization of nullable object
                return $nullableObjectDeserialization($hasObjectType);
            } else {
                return $noOpDeserialization();
            }
        } elseif ($t instanceof Nullable) {
            // Only support deserialization of nullable object types and single json native types for now.
            $nullableType = $t->getActualType();
            if ($nullableType instanceof Object_) {
                return $nullableObjectDeserialization($nullableType); // We know nullableType is an instance of object here
            } elseif (TypeConversions::isNativeJson($t)) {
                return "$key: {$nullableDeserializationWrapper($valueRef)},";
            } else {
                throw new Exception("Cannot make deserialization for Nullable type $t. Only nullable single object types or single native types is supported.");
            }
        } elseif ($t instanceof Array_) {
            $vType = $t->getValueType();
            if (TypeConversions::isNativeJson($vType)) {
                return $noOpDeserialization();
            } elseif ($vType instanceof Object_) {
                $vTypeTypescriptTypes = TypeConversions::objectToTypescript($vType); // We now know this must be a single class name
                $deserializationFunctionName = "deserialize{$vTypeTypescriptTypes->getTypes()}";
                $kType = $t->getKeyType();
                if ($kType instanceof String_) {
                    // Perform deserialization of associative array
                    return "{$this->name()}: Object.fromEntries(Object.entries({$objectName}.{$this->name()})" .
                           ".map(([k, v]) => [k, {$deserializationFunctionName}(v)])),";
                } elseif ($kType instanceof Integer) {
                    // Perform deserialization of normal array
                    return "{$this->name()}: {$objectName}.{$this->name()}.map($deserializationFunctionName),";
                } else {
                    throw new Exception("Array key type must be string or integer");
                }
            } else {
                throw new Exception("Array value type must be native json or Object_");
            }
        } elseif ($t instanceof Object_) {
            return $objectDeserialization($this->getTypescriptTypes());
        } else {
            throw new Exception("Cannot make typescript deserialization for type $t");
        }
    }

    /**
     * @throws InvalidProperty
     */
    public function getImportSet(): ImportSet {
        return $this->getTypescriptTypes()->getImportSet();
    }

    public function typescript(): string {
        return "{$this->name()}: {$this->getTypescriptTypes()->typescript()}";
    }
}
