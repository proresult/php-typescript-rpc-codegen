<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

namespace Proresult\PhpTypescriptRpc\Codegen\Typescript;

/** Container class that holds a set of unique imports. */
class ImportSet {
    /** @var Import[] */
    private array $imports = [];
    public function addImport(Import $import): void {
        // Should maybe add more checking here, and throw exception if there already is an existing import with different ModulePath, but same alias(?)
        // Perhaps also throw exception if there is an existing import with different alias ?
        if (!in_array($import, $this->imports)) {
            $this->imports[] = $import;
        }
    }

    /**
     * @param Import[] $imports
     */
    public function addImports(array $imports): void {
        foreach ($imports as $import) {
            $this->addImport($import);
        }
    }

    public function clone(): ImportSet {
        $clone = new ImportSet();
        $clone->addImports($this->getImports());
        return $clone;
    }

    public function concat(ImportSet $other): ImportSet {
        $result = $this->clone();
        $result->addImports($other->getImports());
        return $result;
    }

    /**
     * @return Import[]
     */
    public function getImports(): array {
        return $this->imports;
    }

    public static function empty(): ImportSet {
        return new ImportSet();
    }

    public static function init(Import ...$imports): ImportSet {
        $ret = new ImportSet();
        $ret->addImports($imports);
        return $ret;
    }

    /**
     * Adjust module path prefix of all imports in a copy of this ImportSet and return it.
     *
     * @param ModulePathPrefixAdjustments $adjustment
     *
     * @return ImportSet
     */
    public function withModulePathPrefixAdjusted(ModulePathPrefixAdjustments $adjustment): ImportSet {
        $ret = ImportSet::empty();
        foreach ($this->getImports() as $import) {
            $ret->addImport($import->withModulePathPrefixAdjusted($adjustment));
        }
        return $ret;
    }
}
