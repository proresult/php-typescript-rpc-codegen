<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen\Typescript;

    /**
     * Class ModulePathPrefixAdjustment is used to configure how what the typescript output module path will be.
     *
     * The transpiler creates a module directory hierarchy that matches the namespace directory hierarchy of the input php files.
     *
     * Since the typescript project the output goes into probably have a little different module structure this class adjusts the
     * base of the module path for the typescript files being generated.
     *
     * The remove path will be removed, and the add path will be added on all files transpiled. Look at the tests for examples.
     *
     * @package Proresult\PhpTypescriptRpc\Typescript
     */
class ModulePathPrefixAdjustment {
    public ModulePath $remove;
    public ModulePath $add;

    /**
     * ModulePathPrefixAdjustement constructor.
     *
     * @param ModulePath $remove
     * @param ModulePath $add
     */
    public function __construct(ModulePath $remove, ModulePath $add) {
        $this->remove = $remove;
        $this->add    = $add;
    }

    public static function none(): ModulePathPrefixAdjustment {
        return new ModulePathPrefixAdjustment(new ModulePath([]), new ModulePath([]));
    }

    public function adjust(ModulePath $modulePath): ModulePath {
        if ($modulePath->hasPrefix($this->remove)) {
            return $modulePath->withoutPrefix($this->remove)->withPrefix($this->add);
        } else {
            return $modulePath;
        }
    }
}
