<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen;

    use Iterator;

class PhpFileFilerIterator extends \FilterIterator {
    public function __construct(Iterator $iterator) {
        parent::__construct($iterator);
    }

    /**
     * @inheritDoc
     */
    public function accept(): bool {
        /**
         * @var \SplFileInfo $file
         */
        $file  = $this->getInnerIterator()->current();
        return $file->isFile() && $file->getExtension() == "php";
    }
}
