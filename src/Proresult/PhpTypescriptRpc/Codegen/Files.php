<?php

    /*
    Copyright 2021, Proresult AS.
    License: MIT
    */
    declare(strict_types=1);

    namespace Proresult\PhpTypescriptRpc\Codegen;

    use Exception;
    use FilesystemIterator;
    use Iterator;
    use Proresult\PhpTypescriptRpc\Codegen\Exceptions\ErrorHandlerException;
    use Proresult\PhpTypescriptRpc\Codegen\Exceptions\FileProcessingException;
    use Proresult\PhpTypescriptRpc\Codegen\Exceptions\FileWriteException;
    use RecursiveDirectoryIterator;
    use RecursiveIteratorIterator;
    use SplFileInfo;

class Files {
    public static function rmdirRecursive(string $dir, string $suffixCheck): void {
        if (!is_dir($dir)) {
            return; // Given dir does not exist, so return without doing anything
        }
        $absPath = realpath($dir);
        if ($absPath === false) {
            throw new Exception("could not resolve $dir to a real path. Aborting delete");
        }
        if (count(explode(DIRECTORY_SEPARATOR, $absPath)) < 3) {
            throw new Exception("Will not delete dir that has less than 3 path elements ($absPath)");
        }
        $it = new RecursiveDirectoryIterator($dir, FilesystemIterator::SKIP_DOTS);
        $it = new RecursiveIteratorIterator($it, RecursiveIteratorIterator::CHILD_FIRST);
        /** @var SplFileInfo $file */
        foreach ($it as $file) {
            if ($file->isDir() && !$file->isLink()) {
                rmdir($file->getPathname());
            } elseif ($file->isFile() && !$file->isLink()) {
                if ($file->getExtension() === $suffixCheck) {
                    unlink($file->getPathname());
                } else {
                    throw new Exception("Will not delete file {$file->getPathname()}. It doesn't end with \".$suffixCheck.\"");
                }
            } else {
                throw new Exception("Invalid file handle, will not delete ({$file->getPathname()})");
            }
        }
        rmdir($dir);
    }

    /**
     * @param string $baseDir
     *
     * @return Iterator<SplFileInfo>
     */
    public static function phpFileIterator(string $baseDir): Iterator {
        $all = new RecursiveDirectoryIterator(
            $baseDir,
            FilesystemIterator::CURRENT_AS_FILEINFO | FilesystemIterator::SKIP_DOTS | \FilesystemIterator::KEY_AS_PATHNAME
        );
        $all = new \RecursiveIteratorIterator($all);
        return new PhpFileFilerIterator($all);
    }

    /**
     * @param $baseDir
     *
     * @return SplFileInfo[]
     */
    public static function phpFileArray(string $baseDir): array {
        $files = self::phpFileIterator($baseDir);
        $ret = [];
        foreach ($files as $file) {
            $ret[] = $file;
        }
        return $ret;
    }

    public static function writeFile(string $filePath, string $fileContent): void {
        try {
            // Create missing directories first
            $dirPath = dirname($filePath);
            if (!file_exists($dirPath)) {
                mkdir($dirPath, recursive: true);
            }
            $byts = file_put_contents($filePath, $fileContent, LOCK_EX);
            if ($byts === 0) {
                throw new Exception("zero bytes written to output! Filepath: $filePath");
            }
        } catch (ErrorHandlerException $e) {
            throw new FileWriteException($filePath, "", $e);
        }
    }
}
